<?php

return [
    'product_main_actions' => env('LIMIT_PRODUCT_PRODUCT_MAIN_ACTION', 8),
    'products_page_action' => env('LIMIT_PRODUCT_PAGE_ACTION', 6)
];
