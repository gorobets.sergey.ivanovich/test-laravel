<?php

return [
    'chunk' => env('IMPORT_CHUNK') ?? 40
];
