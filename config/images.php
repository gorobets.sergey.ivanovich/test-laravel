<?php

return [
    'property' => [
        'small' =>  env('IMAGE_SIZE_PROPERTY_CART_SMALL') ?? 48,
        'medium' => env('IMAGE_SIZE_PROPERTY_CART_MEDIUM') ?? 72,
    ]
];
