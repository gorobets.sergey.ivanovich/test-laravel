<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'pub' => [
            'driver' => 'local',
            'root' => public_path(),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
        ],

        'actions' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/actions'),
            'url' => '/storage/images/actions',
        ],

        'news' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/news'),
            'url' => '/storage/images/news',
        ],

        'sliders' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/sliders'),
            'url' => '/storage/images/sliders',
        ],

        'partners' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/partners'),
            'url' => '/storage/images/partners',
        ],

        'products' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/products'),
            'url' => '/storage/images/products',
        ],

        'category_slider' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/category/slider'),
            'url' => '/storage/images/category/slider',
        ],

        'category' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/category'),
            'url' => '/storage/images/category',
        ],

        'property' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/property'),
            'url' => '/storage/images/property',
        ],

        'editor' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/editor'),
            'url' => '/storage/images/editor',
        ],

        'import' => [
            'driver' => 'local',
            'root' => storage_path('app/public/import'),
            'url' => '/storage/import',
        ],

        'media' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/media'),
            'url' => '/storage/images/media',
        ],

        'rich' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/rich'),
            'url' => '/storage/images/rich',
        ],

        'archives' => [
            'driver' => 'local',
            'root' => storage_path('app/public/export/archives'),
            'url' => '/storage/export/archives',
        ],

        'solutions' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/solutions'),
            'url' => '/storage/images/solutions',
        ],

        'files' => [
            'driver' => 'local',
            'root' => storage_path('app/public/files'),
            'url' => '/storage/files',
        ],

        'preset' => [
            'driver' => 'local',
            'root' => storage_path('app/public/preset'),
            'url' => '/storage/preset',
        ],

        'ftp' => [
            'driver' => 'ftp',
            'host' => env('FTP_PRICE_HOST'),
            'username' => env('FTP_PRICE_USERNAME'),
            'password' => env('FTP_PRICE_PASSWORD'),
            'port' => env('FTP_PRICE_PORT'),
            'ignorePassiveAddress' => true,
            'passive' => 'false',
            // Optional FTP Settings...
            // 'port' => 21,
            // 'root' => '',
            // 'passive' => true,
            // 'ssl' => true,
            // 'timeout' => 30,
        ],

        'do' => [
            'driver' => 's3',
            'key' => env('DO_SPACE_KEY'),
            'secret' => env('DO_SPACE_SECRET'),
            'region' => env('DO_SPACE_REGION'),
            'bucket' => env('DO_SPACE_BUCKET'),
            'url' => env('DO_SPACE_URL'),
        ],

        'news_category' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/news_category'),
            'url' => '/storage/images/news_category',
        ],

        'product_rich_content' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/product_rich_content'),
            'url' => '/storage/images/products',
        ],

        'rich_content_layout' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/rich_content_layout'),
            'url' => '/storage/images/rich_content_layout',
        ],

        'authors' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/authors'),
            'url' => '/storage/images/authors',
        ],

        'landing_cases' => [
            'driver' => 'local',
            'root' => storage_path('app/public/images/landing/landing_cases'),
            'url' => '/storage/images/landing/landing_cases',
        ],

        'suppliers' => [
            'driver' => 'local',
            'root' => storage_path('app/public/import/suppliers'),
            'url' => '/storage/import/suppliers',
        ],
    ],

];
