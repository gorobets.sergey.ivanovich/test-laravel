<?php

return [
    'throttle' => env('ARCHIVE_THROTTLE') ?? '1,10',
];
