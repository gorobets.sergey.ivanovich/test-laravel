<?php

return [
    'index' => env('APP_ENV') ?? 'local',
    'fb_pixel' => env('FB_PIXEL') ?? 0
];
