<?php

return [
    'api_key' => env('YOUTUBE_API_KEY') ?? '',
    'channel_id' => env('YOUTUBE_CHANNEL_ID') ?? '',
    'url_search' => env('YOUTUBE_URL_SEARCH') ?? '',
    'url_watch' => env('YOUTUBE_URL_WATCH') ?? ''
];
