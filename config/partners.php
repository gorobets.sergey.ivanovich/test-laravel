<?php

return [
    'product_per_page' => env('API_PARTNER_PRODUCT_PER_PAGE') ?? 15
];
