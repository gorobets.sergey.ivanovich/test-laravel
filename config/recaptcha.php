<?php

return [
    'enabled' => env('VUE_APP_RECAPTCHA_ENABLED', true),
    'key'     => env('VUE_APP_RECAPTCHA_SITE_KEY'),
    'secret'  => env('VUE_APP_RECAPTCHA_SECRET_KEY'),
];
