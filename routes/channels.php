<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

 \Illuminate\Support\Facades\Broadcast::channel('user.{id}', function ($user, $id) {
     \Illuminate\Support\Facades\Log::info(9999);
     return $user->id === (int) $id;
 });
