<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix'  => \App\Services\LaravelLocalization::setLocale(),
], function () {
    Route::get('/sitemap.html', 'Client\SitemapController')->name('pages.sitemap');
});

Route::get('/callback/tiktok', function () {
    return response()->json([], 200);
})->name('callback.tiktok');

Route::group([
    'middleware' => ['throttle:10,1']
], function () {
    Route::post('/alfabank/callback', 'Client\AlfaBankController@callback')->name('alfabank.callback');
});

Route::get('/document', 'Client\ClientController@document')->name('pages.document');

Route::post('/user/reset-password', 'Auth\ResetPasswordController@reset')->name('user.reset.password');
Route::get('/user/verify/{token}', 'Auth\ResetPasswordController@verify')->name('user.verify.password');

Route::get('/document', 'Client\ClientController@document')->name('pages.document');

Auth::routes();

Route::get('/locale/{locale}', 'Controller@setLocale')->name('locale');

Route::group([
    'prefix'  => 'admin',
], function () {
    Route::group([
        'prefix'  => 'architecture',
    ], function () {
        Route::get('/category', 'SpaController@index')->name('architecture.category');
        Route::get('/feature', 'SpaController@index')->name('architecture.feature');
        Route::get('/order-management', 'SpaController@index')->name('architecture.order_management');
        Route::get('/subcategory', 'SpaController@index')->name('architecture.subcategory');
        Route::get('/presets', 'SpaController@index')->name('architecture.presets');
    });
});

Route::group([
    'prefix'  => \App\Services\LaravelLocalization::setLocale(),
    'middleware' => \App\Http\Middleware\Service::class
], function () {
    Route::group([
        'prefix'  => \App\Services\CityLocation::setLocale(),
    ], function () {

        Route::get('/', 'Client\ClientController@home')->name('index');

        Route::get('catalog/{category_slug}/{filters?}', 'Client\CatalogController@index')
            ->where('filters', '.*')->name('client.catalog.index')->middleware('redirect_301');

        Route::get('/news/{news_slug}', 'Client\NewsController@show')->name('client.news.show')->middleware('redirect_301');
        Route::get('/news/{news_slug}/amp', 'Client\NewsController@showAmp')->name('client.news.show.amp')->middleware('redirect_301');

        Route::get('/cart', 'Client\ClientCartController@cart')->name('cart');
    });
});

Route::group([
    'middleware' => ['throttle:'.config('archives_throttle.throttle')]
], function () {
    Route::get('/product/image/archive/{product_slug}', 'ProductArchiveController@archive')->name('product.archive.download');
});

