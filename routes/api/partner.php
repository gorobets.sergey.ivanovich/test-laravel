<?php

Route::post('/logout', 'Auth\AdminLoginController@logout')->name('logout');

Route::group([
    'prefix'  => 'auth',
], function () {
    Route::post('/login', 'Auth\LoginController@login')->name('partner.login');
});

Route::post('/refresh/token', 'Auth\AdminLoginController@refreshToken')->name('refresh.token');

Route::group([
    'middleware' => 'partner'
], function () {

    Route::group([
        'prefix'  => 'product',
    ], function () {
        Route::get('/index', 'ProductController@index')->name('partner.product.index');
        Route::get('/export', 'ProductController@export')->name('partner.product.export');
        Route::get('/export/file/{archive}', 'ProductController@exportFile')->name('partner.product.export.file');
        Route::get('/export/download', 'ProductController@download')->name('partner.product.download');
    });

    Route::group([
        'middleware' => ['partner-api', 'throttle:60,1']
    ], function () {
        Route::get('/get-categories', 'CategoryController@list')->name('category.partner.list');
        Route::get('/{category}/get-list-product', 'ProductController@list')->name('category.partner.products.list');
        Route::get('/{product}/get-product-detail', 'ProductController@showPartner')->name('category.partner.product.info');
        Route::get('/{category}/get-property-list', 'CategoryController@propertyList')->name('category.property.list');
        Route::get('/{category}/product-static-info', 'ProductController@listStaticInfo')->name('category.partner.products.list.static-info');
        Route::get('/category/{category}/property', 'CategoryController@propertyDetailList')->name('category.partner.property.list');
        Route::get('/products/availability', 'ProductController@availability')->name('partner.products.availability');
    });
});
