<?php

Route::post('/login', 'Auth\AdminLoginController@login')->name('login.submit');

Route::group([
    'middleware' => ['onees']
], function () {
    Route::post('/price/import', 'PriceController@importXml')->name('onees.proce.import');
    Route::post('/logout', 'Auth\AdminLoginController@logout')->name('onees.logout');
});
