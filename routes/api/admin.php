<?php
use Illuminate\Support\Facades\Route;

Route::post('/login', 'Auth\AdminLoginController@login')->name('login.submit');

Route::post('/logout', 'Auth\AdminLoginController@logout')->name('logout');

Route::post('/refresh/token', 'Auth\AdminLoginController@refreshToken')->name('refresh.token');

Route::post('/editor/image', 'ImageUploadController@upload')->name('editor.image');

Route::group([
    'middleware' => ['access']
], function () {

    Route::group([
        'prefix'  => 'page',
    ], function () {
        Route::get('/index', 'PageController@index')->name('page.index');
        Route::get('/show', 'PageController@show')->name('page.show');
        Route::put('/update', 'PageController@update')->name('page.update');
    });

});
