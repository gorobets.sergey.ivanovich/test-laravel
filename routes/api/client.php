<?php

Route::group([
    'prefix'  => 'user',
], function () {
    Route::post('/register', 'Auth\RegisterController@create')->name('user.register');
});
Route::group([
    'namespace' => 'Auth'
], function () {

    Route::group([
        'prefix'  => 'auth',
    ], function () {
        Route::post('/login', 'ClientLoginController@login')->name('user.login');
        Route::post('/logout', 'ClientLoginController@logout')->name('user.logout');
        Route::post('/refresh/token', 'ClientLoginController@refreshToken')->name('user.refresh.token');
        Route::post('/reset/password', 'ResetPasswordController@reset')->name('user.reset.password');
        Route::put('/user/reset-update-password', 'ResetPasswordController@update')->name('user.update.password.email');
        Route::put('/user/update-password', 'ResetPasswordController@updatePassword')->name('user.update.password');
        Route::put('/user/update-login', 'ResetLoginController@update')->name('user.update.login');
        Route::post('/verify/phone_code', 'ResetPasswordController@verifyPhoneCode')->name('user.verify.phone.cod');
        Route::get('/{service}', 'SocialAuthController@redirectToProvider');
        Route::get('/{service}/callback', 'SocialAuthController@handleProviderCallback');
    });

    Route::group([
        'prefix'  => 'profile',
        'middleware' => ['client']
    ], function () {
        Route::get('/', 'ProfileController@index')->name('user.profile.index');
        Route::post('/create', 'ProfileController@create')->name('user.profile.create');
        Route::put('/update', 'ProfileController@update')->name('user.profile.update');
    });

    Route::group([
        'prefix'  => 'cart',
    ], function () {
        Route::get('/', 'UserCartController@index')->name('user.cart.index');
        Route::get('/link', 'UserCartController@link')->name('user.cart.link');
        Route::put('/add', 'UserCartController@add')->name('user.cart.add');
        Route::put('/combine', 'UserCartController@combine')->name('user.cart.combine');
        Route::delete('/', 'UserCartController@clear')->name('user.cart.clear');
    });

    Route::group([
        'prefix'  => 'order',
    ], function () {
        Route::get('/', 'ClientOrderController@index')->name('client.order.index');
        Route::post('/create', 'ClientOrderController@create')->name('client.order.create');
        Route::get('/history/{order_client}', 'ClientOrderHistoryController')->name('client.order.history');
    });
});

Route::group([
    'prefix'  => \App\Services\LaravelLocalization::setLocale(),
], function () {
        Route::get('/catalog/{category_slug}/pagination/yet/{filters?}', 'CatalogController@yet')
            ->where('filters', '.*')
            ->name('category.pagination.yet');
        Route::get('/catalog/{category_slug}/count/{filters?}', 'CatalogController@countWithFilters')
            ->where('filters', '.*')
            ->name('category.products.count');

        Route::get('/search/result/pagination/yet', 'CatalogController@searchYet')
            ->where('filters', '.*')
            ->name('search.pagination.yet');

});

