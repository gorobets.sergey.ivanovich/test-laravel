<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\CeoPageTitle;
use App\Models\GoogleCounter;
use App\Models\Multifilter;
use App\Models\Page;
use App\Models\Contact;
use App\Models\Product;
use App\Repository\City\CityQuery;
use App\Repository\Faq\FaqRepository;
use App\Repository\Media\MediaQueryRepository;
use App\Repository\Product\QueryProductRepository;
use App\Repository\PropertyValueTranslate\PvtQuery;
use App\Services\Anker\Anker;
use App\Services\Landing\Landing;
use App\Services\PresetData\PresetData;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * @var CityQuery
     */
    private $cityQuery;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            [
                'pages.category-catalog',
                'layouts.client',
                'pages.sitemap',
            ],
            function ($view) {
                $view->with([
                    'presets' => Cache::store('redis')
                        ->remember('cache:menu:preset_'.app()->getLocale().'_'.config()->get('app.city'), 3600, function () {
                            return (new PresetData())->getMenuData();
                        }),
                    'preOrdersProducts' => Cache::store('redis')
                        ->remember('cache:product:preorder_'.app()->getLocale(),3600, function () {
                            return (new QueryProductRepository)->getProductPreOrder();
                        }),
                    'deadlineProducts' => Cache::store('redis')
                        ->remember('cache:product:deadline_'.app()->getLocale(), 3600, function () {
                            return (new QueryProductRepository)->getProductDeadline();
                        }),
                    'customProducts' => Cache::store('redis')
                        ->remember('cache:product:custom_'.app()->getLocale(), 3600, function () {
                            return (new QueryProductRepository)->getProductCustom();
                        }),
                    'youtubeData' => (new \App\Services\Youtube\Youtube())->index(),
                ]);
            }
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
