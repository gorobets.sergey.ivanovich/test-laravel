<?php

namespace App\Providers;

use App\Services\AmpFeed\AmpFeed;
use App\Services\AmpFeed\AmpFeedContract;
use App\Services\FbFeed\FbFeed;
use App\Services\FbFeed\FbFeedContract;
use App\Services\HotlineFeed\HotlineFeed;
use App\Services\GoogleFeed\GoogleFeed;
use App\Services\HotlineFeed\HotlineFeedContract;
use App\Services\GoogleFeed\GoogleFeedContract;
use App\Services\Sitemap\SitemapService;
use App\Services\Sitemap\SitemapServiceImpl;
use Illuminate\Support\ServiceProvider;

/**
 * Class InjectServiceProvider
 *
 * @package App\Providers
 */
final class InjectServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SitemapService::class, SitemapServiceImpl::class);
        $this->app->bind(HotlineFeedContract::class, HotlineFeed::class);
        $this->app->bind(GoogleFeedContract::class, GoogleFeed::class);
        $this->app->bind(FbFeedContract::class, FbFeed::class);
        $this->app->bind(AmpFeedContract::class, AmpFeed::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
