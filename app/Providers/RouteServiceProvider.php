<?php

namespace App\Providers;

use App\Models\{Action,
    FilterPattern,
    GuaranteeItem,
    Media,
    News,
    NewsCategory,
    Order,
    Partner,
    Product,
    Property,
    Slider,
    Category,
    Subscription};
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * This namespace is applied to your admin controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespaceAdmin = 'App\Http\Controllers\Admin';

    /**
     * This namespace is applied to your admin controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespaceAdminApi = 'App\Http\Controllers\Admin\Api';

    /**
     * This namespace is applied to your admin controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespaceClientApi = 'App\Http\Controllers\Client\Api';

    /**
     * This namespace is applied to your admin controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespacePartnerApi = 'App\Http\Controllers\Partner\Api';

    /**
     * This namespace is applied to your admin controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespaceOneEsClientApi = 'App\Http\Controllers\OneEsClient\Api';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Route::bind('action', function ($id) {
            return Action::find($id);
        });

        Route::bind('news_slug', function ($news_slug) {
            return News::with('translate')->where('slug', $news_slug)->first();
        });

        Route::bind('category_news_slug', function ($category_slug) {
            return NewsCategory::with('translate')->where('slug', $category_slug)->first();
        });

        Route::bind('slider', function ($id) {
            return Slider::find($id);
        });

        Route::bind('partner', function ($id) {
            return Partner::find($id);
        });

        Route::bind('category_slug', function ($category_slug) {
            return Category::with('slider', 'translate', 'slider.translate')->where('slug', $category_slug)->first();
        });

        Route::bind('category', function ($category) {
            return Category::where('id', $category)->first();
        });

        Route::bind('filters', function ($filters, \Illuminate\Routing\Route $route) {
            // Special convert for catalog filters to array
            // url : /propertyTitle=value1-or-value2
            // result : ['propertyTitle' => ['value1','value2']]
            if ($route->getName() === 'client.catalog.index') {
                return [
                    'original' => $filters,
                    'params' => array_reduce(explode('/', $filters), function ($all, $item) {
                        $param = explode('=', $item);
                        if (count($param) === 2) {
                            $all[$param[0]] = explode('-or-', $param[1]);
                        }
                        return $all;
                    }, []),
                ];
            }
            return $filters;
        });

        Route::bind('product_slug', function ($product_slug) {
            return Product::with('gallery', 'parameters', 'actions', 'actions.translate', 'translate',
                'parameters.property', 'parameters.values', 'parameters.values.partner', 'product_relations_main', 'product_relations_main.product_liasion',
                'product_relations_second.product', 'product_relations_second', 'rich', 'layout_rich', 'layout_rich.content',
                'layout_rich.content.translate')
                ->where('slug', $product_slug)->first();
        });

        Route::bind('sale_slug', function ($sale_slug) {
            return Action::with('translate')->where('slug', $sale_slug)->first();
        });

        Route::bind('property', function ($id) {
            return Property::find($id);

        });

        Route::bind('feedback', function ($id) {
            return Feedback::find($id);
        });

        Route::bind('product', function ($id) {
            return Product::with('parameters', 'translate', 'parameters.property', 'parameters.values', 'parameters.values.partner')
                ->where('id', $id)->first();
        });

        Route::bind('subscription', function ($id) {
            return Subscription::find($id);
        });

        Route::bind('order', function ($order) {
            return Order::find($order);
        });

        Route::bind('order_client', function ($order) {
            return Auth('client-api')->user()->order()->where('client_order_id', $order)->first();
        });

        Route::bind('filter', function ($order) {
            return FilterPattern::find($order);
        });

        Route::bind('guarantee', function ($order) {
            return GuaranteeItem::find($order);
        });

        Route::bind('media', function ($media) {
            return Media::find($media);
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapWebAdminRoutes();

        $this->mapApiAdminRoutes();

        $this->mapApiClientRoutes();

        $this->mapApiPartnerRoutes();

        $this->mapApiOneEsClientRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "admin web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebAdminRoutes()
    {
        Route::middleware('web')
            ->as('admin.')
            ->namespace($this->namespaceAdmin)
            ->group(base_path('routes/web/admin.php'));
    }


    /**
     * Define the "admin api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiAdminRoutes()
    {
        Route::prefix('api/admin')
            ->as('api.admin.')
            ->namespace($this->namespaceAdminApi)
            ->group(base_path('routes/api/admin.php'));
    }

    /**
     * Define the "admin api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiClientRoutes()
    {
        Route::prefix('api/client')
            ->as('api.client.')
            ->namespace($this->namespaceClientApi)
            ->group(base_path('routes/api/client.php'));
    }

    /**
     * Define the "partner api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiPartnerRoutes()
    {
        Route::prefix('api/partner')
            ->as('api.partner.')
            ->namespace($this->namespacePartnerApi)
            ->group(base_path('routes/api/partner.php'));
    }


    protected function mapApiOneEsClientRoutes()
    {
        Route::prefix('api/price_client')
            ->as('api.one_es_client.')
            ->namespace($this->namespaceOneEsClientApi)
            ->group(base_path('routes/api/one_es.php'));
    }
}
