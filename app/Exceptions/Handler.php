<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if(get_class($exception) != 'Illuminate\Validation\ValidationException'){
            if($this->isHttpException($exception)){

                $code = $exception->getStatusCode();

                If($code === 404){
                    return response()->view('404', [], 404);
                }
                If($code === 500){
                    if($request->is('api/admin/*')){
                        return response($exception->getMessage(), 402);
                    }
                }
            }else{
                if($exception->getMessage() == "Unauthenticated."){
                    return response('', 401);
                }

                if($request->is('api/admin/*')){
                    return response($exception->getMessage(), 402);
                }

                return response()->view('500',[],500);
            }
        }

        return parent::render($request, $exception);
    }
}
