<?php

namespace App\Jobs;

use App\Mail\ServiceShipped;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendMessageService implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $item;
    public $timeout = 900;


    /**
     * SendMessageService constructor.
     * @param $item
     */
    public function __construct($item)
    {
        $this->item = $item;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to([config('mail.to'),config('mail.help')])
            ->send(new ServiceShipped($this->item));

        return;
    }
}
