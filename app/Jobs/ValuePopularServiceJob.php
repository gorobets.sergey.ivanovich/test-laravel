<?php

namespace App\Jobs;

use App\Services\PropertyValuePopular\PropertyValuePopularService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ValuePopularServiceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 900;
    public $valuePopularService;
    public $paramsFilters;
    public $ip;
    public $category;

    public function __construct($paramsFilters, $ip, $category)
    {
        $this->paramsFilters = $paramsFilters;
        $this->ip = $ip;
        $this->category = $category;
        $this->valuePopularService = new PropertyValuePopularService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->valuePopularService->initialFromFilters($this->paramsFilters, $this->ip, $this->category);

        return;
    }
}

