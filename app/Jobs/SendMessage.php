<?php

namespace App\Jobs;

use App\Mail\OrderShipped;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $order;
    public $url;
    public $timeout = 900;


    /**
     * SendMessage constructor.
     * @param $order
     * @param $url
     */
    public function __construct($order, $url)
    {
        $this->order = $order;
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $to = config('mail.to');
        Mail::to($to)->send(new OrderShipped($this->order, $this->url));

        return;
    }
}
