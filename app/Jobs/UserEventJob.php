<?php

namespace App\Jobs;

use App\Mail\UserRegister;
use App\Mail\UserResetPassword;
use App\Mail\UserUpdatePassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class UserEventJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $event;
    private $email;
    private $name;
    private $surname;
    private $token;

    /*
     *
     */
    public function __construct($event, $email = null, $name = null, $surname = null, $token = null)
    {
        $this->event = $event;
        $this->email = $email;
        $this->name = $name;
        $this->surname = $surname;
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch ($this->event){
            case 'register':Mail::to($this->email)->send(new UserRegister($this->name, $this->surname));break;
            case 'reset_password':Mail::to($this->email)->send(new UserResetPassword($this->token));break;
            case 'update_password':Mail::to($this->email)->send(new UserUpdatePassword());break;
        }
    }

}
