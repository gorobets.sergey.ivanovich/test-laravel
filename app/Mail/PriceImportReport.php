<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PriceImportReport extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * OrderShipped constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Изменение цен "7%"')->markdown('emails.price_import_report',[
            'data' => $this->data,
        ]);
    }
}
