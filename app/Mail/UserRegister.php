<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegister extends Mailable
{
    use Queueable, SerializesModels;

    private $name;
    private $surname;

    /**
     * OrderShipped constructor.
     * @param $order
     * @param $url
     */
    public function __construct($name, $surname)
    {
        $this->name = $name;
        $this->surname = $surname;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Register')->markdown('emails.user.register',[
            'name' => $this->name,
            'surname' => $this->surname
        ]);
    }
}
