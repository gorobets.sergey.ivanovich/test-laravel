<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $url;

    /**
     * OrderShipped constructor.
     * @param $order
     * @param $url
     */
    public function __construct($order, $url)
    {
        $this->order = $order;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Новый заказ')->markdown('emails.orders.shipped',[
            'order' => $this->order,
            'url' => $this->url
        ]);
    }
}
