<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DaasShipped extends Mailable
{
    use Queueable, SerializesModels;

    public $item;

    /**
     * OrderShipped constructor.
     * @param $item
     */
    public function __construct($item)
    {
        $this->item = $item;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Новый запрос аренды')->markdown('emails.daas',[
            'item' => $this->item,
        ]);
    }
}
