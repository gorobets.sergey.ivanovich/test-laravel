<?php


namespace App\Helpers;


use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class Noindex
{
    public static function set($no_index)
    {
        if ($no_index) {
            return true;
        }
        $array = array_reverse(explode('/', url()->current()));

        foreach ($array as $item) {
            $data = explode('=', $item);

            if(count($data) === 2 && $data[0] === 'sort'){
                return true;
            }
        }

        return false;
    }

    public static function fromUrl():bool
    {
        return Str::contains(url()->current(), ['actions='])
            || in_array(Route::currentRouteName(), ['pages.personal', 'pages.orders', 'pages.baske', 'pages.my-basket', 'pages.orders', 'pages.ordering', 'cart']);
    }

    /**
     * @return bool
     */
    public static function noindexNofollow():bool
    {
        return request()->query->count() > 0 && !in_array('page', request()->query->keys());
    }

    /**
     * @return bool
     */
    public static function noindexFollow():bool
    {
        return request()->query->count() === 1 && in_array('page', request()->query->keys());
    }
}
