<?php

namespace App\Helpers;

class BreadcrumbsCEO
{
    public static function get($data)
    {
        $item = [
            "@context" => "https://schema.org",
            "@type" => "BreadcrumbList",
            "itemListElement" => [
                [
                    "@type" => "ListItem",
                    "position" => 1,
                    "name" => __('pages.main.title'),
                    "item" => route('index')
                ]
            ]
        ];

        $count = count($data);
        $position = 1;
        foreach ($data as $datum) {
            if($position != $count){
                $item['itemListElement'][] = [
                    "@type" => "ListItem",
                    "position" => ++$position,
                    "name" => str_replace('"', '&quot;', $datum['title']),
                    "item" => $datum['route']
                ];
            }else{
                $item['itemListElement'][] = [
                    "@type" => "ListItem",
                    "position" => ++$position,
                    "name" => str_replace('"', '&quot;', $datum['title']),
                ];
            }
        }

        $str = preg_replace_callback(
            '/\\\u([a-f0-9]{4})/i',
            function ($m) {
                return chr(hexdec($m[1])-1072+224);
            },
            json_encode($item)
        );

        $str = str_replace('\\', '', mb_convert_encoding($str, "utf-8", "cp1251"));

        $simbols = [
            "\x06" => "i",
            "\x07" => "ї",
            "\x04" => "є",
        ];
        foreach ($simbols as $x => $letter){
            $str = str_replace($x,$letter,$str);
        }

        return $str;
    }
}
