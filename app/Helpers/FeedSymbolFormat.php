<?php

namespace App\Helpers;

class FeedSymbolFormat
{
    public static function format($str)
    {
        $simbols = [
            "<" => "&lt;",
            ">" => "&gt;",
            "'" => "&apos;",
            "\"" => "&quot;",
        ];
        foreach ($simbols as $x => $letter){
            $str = str_replace($x,$letter,$str);
        }

        return $str;
    }
}
