<?php

namespace App\Helpers;

class FaqHelper
{
    public static function get($data)
    {
        $mainEntity = [];
        foreach ($data as $datum) {
            $mainEntity[] = [
                "@type" => "Question",
                "name" => str_replace('"', '\'',$datum['question']),
                "acceptedAnswer" => [
                    "@type" => "Answer",
                    "text" => str_replace('"', '\'',$datum['answer'])
                ]
            ];
        }
        $item = [
            "@context" => "https://schema.org",
            "@type" => "FAQPage",
            "mainEntity" => $mainEntity
        ];

        $str = preg_replace_callback(
            '/\\\u([a-f0-9]{4})/i',
            function ($m) {
                return chr(hexdec($m[1])-1072+224);
            },
            json_encode($item)
        );

        $str = str_replace('\\', '', mb_convert_encoding($str, "utf-8", "cp1251"));

        $simbols = [
            "\x06" => "i",
            "\x07" => "ї",
            "\x04" => "є",
        ];
        foreach ($simbols as $x => $letter){
            $str = str_replace($x,$letter,$str);
        }

        return $str;
    }
}
