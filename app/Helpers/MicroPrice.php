<?php

namespace App\Helpers;

class MicroPrice
{
    public static function image(array $products = []): string
    {
        $images = [];

        foreach ($products as $product) {
            $img = array_reverse(explode('/',$product['image']));
            $images[] = '"' . implode('/', [
                env('APP_URL'),
                'storage/images/products',
                $product['id'],
                'gallery',
                $img[1],
                '600_'.implode('_', array_slice(explode('_', $img[0]), 1))
            ]) . '"';

            if (count($images) > 4) break;
        }

        return implode(',',$images);
    }


}
