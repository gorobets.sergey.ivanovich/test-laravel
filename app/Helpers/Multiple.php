<?php

namespace App\Helpers;

class Multiple {

    public static function days($num, $lang) {
        $suff['ru'] = [
            'дней',
            'день',
            'дня',
            'дня',
            'дня',
            'дней',
            'дней',
            'дней',
            'дней',
            'дней',
        ];
        $suff['uk'] = [
            'днів',
            'день',
            'дня',
            'дня',
            'дня',
            'днів',
            'днів',
            'днів',
            'днів',
            'днів',
        ];

        return $suff[$lang][str_split($num)[strlen($num) - 1]];
    }

    public static function import($num) {
        if(!$num){
            return '';
        }

        $suff = [
            'значений',
            'значение',
            'значения',
            'значения',
            'значения',
            'значений',
            'значений',
            'значений',
            'значений',
            'значений',
            'значений',
        ];

        $suff = [
            'значень',
            'значення',
            'значення',
            'значення',
            'значення',
            'значень',
            'значень',
            'значень',
            'значень',
            'значень',
            'значень',
        ];


        if($num < 11){
            return $suff[$num];
        }elseif ($num > 10 && $num < 21){
            return $suff[0];
        }

        return $suff[str_split($num)[strlen($num) - 1]];
    }
}
