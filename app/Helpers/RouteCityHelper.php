<?php

namespace App\Helpers;

class RouteCityHelper
{
    public static function url($url, $str) {
        return implode("/",array_slice(explode("/", $url), 0,3))
            . (app()->getLocale() == "ru" ? "/" : "/uk/").$str;
    }
}
