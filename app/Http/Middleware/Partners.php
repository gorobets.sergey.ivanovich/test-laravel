<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class Partners
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user('admin-api');

        if($user && ($user->role_id == \App\Models\Admin::PARTNER_ROLE)){
            $response = $next($request);

            return $response->header('user_id', $user->id)
                ->header('role_id', $user->role_id)
                ->header('name', $user->name);
        }

        return response('Unauthorized.', 401);
    }
}
