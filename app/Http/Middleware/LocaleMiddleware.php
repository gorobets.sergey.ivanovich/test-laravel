<?php
namespace App\Http\Middleware;

use Closure;
use App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class LocaleMiddleware
{
    public function handle($request, Closure $next)
    {
        if (Session::has('locale')) {
            app()->setLocale(Session::get('locale'));
        }

        return $next($request);
    }
}
