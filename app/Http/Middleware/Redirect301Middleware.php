<?php

namespace App\Http\Middleware;

use App\Models\SlugLog;
use Illuminate\Http\Request;
use Closure;

class Redirect301Middleware
{
    /**
     * Simple rules for redirect
     *
     * @var \string[][]
     */
    protected static $routes = [
        'client.catalog.index' => [
            'slug' => 'category_slug',
            'model' => 'App\Models\Category',
            'filter' => 'filters',
        ],
        'client.product.index' => [
            'slug' => 'product_slug',
            'model' => 'App\Models\Product',
            'filter' => 'filters',
        ],
        'client.case.index' => [
            'slug' => 'product_slug',
            'model' => 'App\Models\Product',
            'filter' => 'filters',
        ],
        'client.news.show' => [
            'slug' => 'news_slug',
            'model' => 'App\Models\News',
            'filter' => 'filters',
        ],
        'client.sale.show' => [
            'slug' => 'sale_slug',
            'model' => 'App\Models\Action',
            'filter' => 'filters',
        ],
        'client.news.show.amp' => [
            'slug' => 'news_slug',
            'model' => 'App\Models\News',
            'filter' => 'filters',
        ],
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * Url for redirect with 301 or
         *
         * @var ?string $redirectUrl
         */
        $redirectUrl = null;

        /**
         * Route name
         *
         * @var string $routeName
         */
        $routeName = $request->route()->getName();

        if (array_key_exists($routeName, self::$routes)) {
            # Unique step for Category
            if ($routeName === 'client.catalog.index') {
                // Object of Category or null
                $catalog = $request->route()->parameter(self::$routes[$routeName]['slug']);
                // filters - array or null // converted in RouteProvider
                $filters = $request->route()->parameter(self::$routes[$routeName]['filter']);
                // When we haven't category and filter
                if (null === $catalog && null === $filters) {
                    $this->getRedirectUrl($routeName, $request,   $redirectUrl);
                // When not find category but have filters
                } elseif (null === $catalog && null !== $filters) {
                    $this->getRedirectUrl($routeName, $request,   $redirectUrl);
                    $redirectUrl .= '/'.$filters['original'];
                // When have category and filter
                } elseif (null !== $catalog && is_array($filters)) {
                    // Flag for redirect
                    $needRedirect = false;
                    // Prepare url variable
                    $url = null;
                    // Need only one property because only one property with one property-value are indexed
                    // Example variable $filters - ['params' => ['property1' => ['value1', 'value2'], ...]
                    if (count($filters['params']) === 1) {
                        // Slug of "Property" (from url)
                        $attr = array_key_first($filters['params']);
                        // Check number of property-value - because need only one property-value
                        if (is_array($filters['params'][$attr]) && count($filters['params'][$attr]) === 1) {
                            // Slug of "Property-value" (from url)
                            $attrValue = $filters['params'][$attr][0];

                            // Eloquent model Property
                            $property = $catalog->propertiesMain()->where('slug', $attr)->first();
                            if (!$property) {
                                // When not found property check old slug
                                $oldSlugLog = $this->getNewSlugLog('App\\Models\\Property', $attr);
                                if ($oldSlugLog && $property = $oldSlugLog->target) {
                                    // Set new slug of property
                                    $attr = $property->slug;
                                    // Change flag of redirect
                                    $needRedirect = true;
                                }
                            }

                            if ($property) {
                                // Eloquent model Property-value
                                $propertyValue = $property->valueTranslate()->where('slug', $attrValue)->first();
                                if (!$propertyValue) {
                                    // When not found property-value check old slug
                                    $oldSlugLog = $this->getNewSlugLog('App\\Models\\PropertyValuesTranslate', $attrValue);
                                    if ($oldSlugLog && $propertyValue = $oldSlugLog->target) {
                                        // Set new slug of property-value
                                        $attrValue = $propertyValue->slug;
                                        // Change flag of redirect
                                        $needRedirect = true;
                                    }
                                }
                            }
                            // put to route real slugs
                            $url = route($routeName, [
                                self::$routes[$routeName]['slug'] => $catalog->slug
                            ]) . '/' . $attr . '=' . $attrValue;
                        }
                    }
                    // When flag is checked - set url for redirect
                    if ($needRedirect) {
                        $redirectUrl = $url;
                    }
                }
            } else {
            # Default step fot other models
                // Object of Model or null
                $model = $request->route()->parameter(self::$routes[$routeName]['slug']);
                // if object is not find we search old slug and setup url with new slug
                if (null === $model) {
                    $this->getRedirectUrl($routeName, $request,   $redirectUrl);
                }
            }
        }

        // redirect to url
        if ($redirectUrl) {
            return response()->redirectTo($redirectUrl, 301);
        }

        return $next($request);
    }

    /**
     * Get new url by old slug
     *
     * @param string $routeName
     * @param Request $request
     * @param ?string $redirectUrl
     *
     * @return void
     */
    protected function getRedirectUrl(string $routeName, Request $request, ?string &$redirectUrl): void
    {
        $log = $this->getSlugLog($routeName, $request);
        if ($log && $log->target) {
            $redirectUrl = route($routeName, [
                self::$routes[$routeName]['slug'] => $log->target->slug
            ]);
        }
        unset($log);
    }

    /**
     * @param string $routeName
     * @param Request $request
     * @return mixed
     */
    protected function getSlugLog(string $routeName, Request $request)
    {
        return SlugLog::where('target_type', self::$routes[$routeName]['model'])
            ->where('slug', $request->route()->originalParameter(self::$routes[$routeName]['slug']))
            ->first();
    }

    /**
     * @param string $model
     * @param $oldSlug
     * @return mixed
     */
    protected function getNewSlugLog(string $model, $oldSlug)
    {
        return SlugLog::where('target_type', $model)
            ->where('slug', $oldSlug)
            ->first();
    }
}
