<?php

namespace App\Http\Middleware;

use Closure;

class RedirectBase
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $baseUrl = explode('?',$request->fullUrl());

        if(in_array('admin', explode('/', $baseUrl[0]))){
            return $next($request);
        }

        if(count($baseUrl) > 1){
            if(mb_strtolower($baseUrl[0]) != $baseUrl[0]){
                $urlResult = $this->urlResult($baseUrl[0]);

                return redirect($urlResult."?".$baseUrl[1], 301);
            }
        }elseif(mb_strtolower($baseUrl[0]) != $baseUrl[0]){
            $urlResult = $this->urlResult($baseUrl[0]);
            return redirect($urlResult, 301);
        }else{
            $urlResult = mb_strtolower($baseUrl[0]);
            $currentUrl = explode('/', $urlResult);

            if (in_array('ru', $currentUrl) || in_array('kiev', $currentUrl)){
                $urlResult = str_replace('/ru','', $urlResult);
                $urlResult = str_replace('/kiev','',$urlResult);

                $urlResult = $this->clearUrlResult($urlResult);
                return redirect($urlResult, 301);
            }elseif(
                (in_array('sales', $currentUrl) && $currentUrl[3] != 'sales' && $currentUrl[3] != 'uk')
                || (in_array('news', $currentUrl) && $currentUrl[3] != 'news' && $currentUrl[3] != 'uk')
                || (in_array('news', $currentUrl) && $currentUrl[3] == 'uk' && $currentUrl[4] != 'news')
                || (in_array('sales', $currentUrl) && $currentUrl[3] == 'uk' && $currentUrl[4] != 'sales')
        ){
                $urlResult = $this->clearUrlResult($urlResult);
                return redirect($urlResult, 301);
            }
        }

        return $next($request);
    }

    private function urlResult(string $url): string
    {
        $urlResult = mb_strtolower($url);
        $currentUrl = explode('/', $urlResult);

        if (in_array('ru', $currentUrl) || in_array('kiev', $currentUrl)){
            $urlResult = str_replace('/ru','', $urlResult);
            $urlResult = str_replace('/kiev','',$urlResult);
        }

        return $this->clearUrlResult($urlResult);
    }

    private function clearUrlResult(string $urlResult): string
    {
        $currentUrl = explode('/', $urlResult);
        if(in_array('sales', $currentUrl)){
            if($currentUrl[3] != 'uk'){
                if($currentUrl[3] != 'sales'){
                    unset($currentUrl[3]);
                }

            }else{
                if($currentUrl[4] != 'sales'){
                    unset($currentUrl[4]);
                }
            }
            $urlResult = implode('/', $currentUrl);
        }elseif(in_array('news', $currentUrl)){
            if($currentUrl[3] != 'uk'){
                if($currentUrl[3] != 'news'){
                    unset($currentUrl[3]);
                }

            }else{
                if($currentUrl[4] != 'news'){
                    unset($currentUrl[4]);
                }

            }
            $urlResult = implode('/', $currentUrl);
        }

        return $urlResult;
    }
}
