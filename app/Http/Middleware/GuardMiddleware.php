<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Closure;

class GuardMiddleware
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->hasHeader('Guard')){
            if($request->header('Guard') == 'admin'){
                Config::set('auth.guards.api.provider', 'admins');
            }elseif($request->header('Guard') == 'one-es-api'){
                Config::set('auth.guards.api.provider', 'oneesclient');
            }elseif($request->header('Guard') == 'client'){
                Config::set('auth.guards.api.provider', 'client');
            }elseif($request->header('Guard') == 'partner'){
                Config::set('auth.guards.api.provider', 'partner');
            }
        }

        if ($request->is('api/admin/*')) {
            Config::set('auth.defaults.guard', 'admin-api');
        }

        if ($request->is('api/one_es_client/*')) {
            Config::set('auth.defaults.guard', 'one-es-api');
        }

        if ($request->is('api/client/auth/*')) {
            Config::set('auth.defaults.guard', 'client-api');
        }
        
        if ($request->is('api/partner/*')) {
            Config::set('auth.defaults.guard', 'partner-api');
        }

        $response = $next($request);

        return $response;
    }
}
