<?php

namespace App\Http\Middleware;

use App\Models\City;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class LocalizationWeb
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $localeDefault = config('app.fallback_locale', 'ru');
        if ($segment = $request->segment(1)) {
            $url = $request->fullUrl();
            $check = false;
            if ($segment === $localeDefault) {
                if (preg_match("/\/$segment\//", $url,  $out)) {
                    $url = str_replace($out[0] , '/', $url);
                    $check = true;
                } elseif (preg_match("/\/$segment/", $url,  $out)) {
                    $url = str_replace($out[0] , '', $url);
                    $check = true;
                }
            } elseif ($segment === 'kiev'){
                if (preg_match("/\/$segment\//", $url,  $out)) {
                    $url = str_replace($out[0] , '/', $url);
                    $check = true;

                } elseif (preg_match("/\/$segment/", $url,  $out)) {
                    $url = str_replace($out[0] , '', $url);
                    $check = true;
                }
            }elseif(Route::currentRouteName() === 'client.product.index'){
                $city = City::where('slug', $segment)->first();

                if($city){
                    if (preg_match("/\/$segment\//", $url,  $out)) {
                        $url = str_replace($out[0] , '/', $url);
                        $check = true;

                    }
                }
            }

            if ($segment = $request->segment(2)) {
                if ($segment === 'kiev'){
                    if (preg_match("/\/$segment\//", $url,  $out)) {
                        $url = str_replace($out[0] , '/', $url);
                        $check = true;

                    } elseif (preg_match("/\/$segment/", $url,  $out)) {
                        $url = str_replace($out[0] , '', $url);
                        $check = true;
                    }
                }elseif(Route::currentRouteName() === 'client.product.index'){
                    $city = City::where('slug', $segment)->first();

                    if($city){
                        if (preg_match("/\/$segment\//", $url,  $out)) {
                            $url = str_replace($out[0] , '/', $url);
                            $check = true;

                        }
                    }
                }
            }

            if($check){
                return redirect($url);
            }

        }

        return $next($request);
    }
}
