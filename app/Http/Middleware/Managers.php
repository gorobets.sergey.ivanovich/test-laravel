<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Admin;

class Managers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user('admin-api');
        if($user && (($user->role_id == Admin::ADMIN_ROLE) || ($user->role_id == Admin::CONTENT_MANAGER_ROLE))){
            $response = $next($request);

            return $response->header('user_id', $user->id)
                ->header('role_id', $user->role_id);
        }

        return response('Unauthorized.', 401);
    }
}
