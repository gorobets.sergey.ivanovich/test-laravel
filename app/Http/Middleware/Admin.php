<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use Closure;
use Illuminate\Support\Facades\Artisan;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user('admin-api');

        if($user && ($user->role_id == \App\Models\Admin::ADMIN_ROLE)){
            $response = $next($request);

            return $response->header('user_id', $user->id)
                ->header('role_id', $user->role_id)
                ->header('shutdown_status', Setting::find(1)->is_active);
        }

        return response('Unauthorized.', 401);
    }
}
