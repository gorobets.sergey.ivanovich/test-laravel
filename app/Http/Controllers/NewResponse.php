<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

/**
 * Trait NewResponse
 *
 * @package App\Http\Controllers
 */
trait NewResponse
{
    /**
     * @param Request $request
     * @param string $updatedAt
     * @param $content
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function newResponse(Request $request, string $updatedAt, $content)
    {
        $updatedAt = date('Y-m-d H:i:s');
        try {
            header("Last-Modified:".date('D, d M Y H:i:s')." GMT");
        }catch (\Exception $e){
            Log::info('error header');
        }

        if ($modifiedSince = $request->header('If-Modified-Since')) {
            preg_match('/(\d+){4}-(\d+){2}-(\d+){2}\ (\d+){2}:(\d+){2}:(\d+){2}/i', $modifiedSince, $parsedTime);
            $updatedAtDate = Carbon::parse($updatedAt);
            $isGtModifiedSince = $updatedAtDate->gt(Carbon::parse($parsedTime[0] ?? date('Y-m-d', strtotime('now'))));

            if (!$isGtModifiedSince) {
                return response($content, 304);
            }
        }

        return $content;
    }
}
