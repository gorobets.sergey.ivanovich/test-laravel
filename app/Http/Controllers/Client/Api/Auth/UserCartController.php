<?php

namespace App\Http\Controllers\Client\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserCart\UserCartAddRequest;
use App\Services\User\UserCart\UserCart;
use Illuminate\Http\JsonResponse;

class UserCartController extends Controller
{
    /**
     * @var UserCart
     */
    private $service;

    public function __construct(UserCart $service)
    {
        $this->service = $service;
        $this->middleware('auth:client-api');
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->service->index();
    }

    /**
     * @return JsonResponse
     */
    public function link(): JsonResponse
    {
        return $this->service->link();
    }

    /**
     * @param UserCartAddRequest $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function add(UserCartAddRequest $request): JsonResponse
    {
        return $this->service->add($request->validated());
    }

    /**
     * @return JsonResponse
     */
    public function clear(): JsonResponse
    {
        return $this->service->clear();
    }

    /**
     * @return JsonResponse
     */
    public function combine(UserCartAddRequest $request): JsonResponse
    {
        return $this->service->combine($request->validated());
    }

}
