<?php

namespace App\Http\Controllers\Client\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Client\Register;
use App\Jobs\EsputnikEventJob;
use App\Jobs\UserEventJob;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(Register $request)
    {
        $data = [
            'name' => $request->name ?? null,
            'email' => $request->email ?? null,
            'phone' => $request->phone ?? null,
            'password' => Hash::make($request->password),
        ];

        try {
            $user = User::create($data);
        }catch (\Exception $e){
            return response()->json($e->getMessage());
        }

        if ($data['email']) {
            UserEventJob::dispatch('register', $data['email'], $data['name']);
            EsputnikEventJob::dispatch('registrationEmail', null, $data['phone'], $data['email']);
        }else{
            EsputnikEventJob::dispatch('registrationPhone', $data['name'], $data['phone']);
        }

        return $user;
    }
}
