<?php

namespace App\Http\Controllers\Client\Api\Auth;

use App\Http\Controllers\Client\Api\ClientApiController;
use App\Http\Resources\AuthClientOrderHistoryResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientOrderHistoryController extends ClientApiController
{
    /**
     * @param Request $request
     * @return JsonResource
     */
    public function __invoke(Request $request): JsonResource
    {
        $history = $request->order_client->order->history;
        return AuthClientOrderHistoryResource::collection($history);
    }

}
