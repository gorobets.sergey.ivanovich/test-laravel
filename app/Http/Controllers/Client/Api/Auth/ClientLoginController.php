<?php

namespace App\Http\Controllers\Client\Api\Auth;

use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Client as PassportClient;

class ClientLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $client;

    /**
     * AdminLoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);

        $client = new PassportClient;
        $this->client = $client->where('name', 'Client')->where('password_client', 1)->first();

        if (!$this->client) {
            abort(400, __('system.passport'));
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('auth.admin-login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string|void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        $credential = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (
            method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)
        ) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if (Auth::guard('client')->attempt($credential, $request->remember)) {
            $params = [
                'grant_type'    => 'password',
                'client_id'     => $this->client->id,
                'client_secret' => $this->client->secret,
                'username'      => trim($request->email),
                'password'      => trim($request->password),
                'scope'         => '*',
            ];

            $http = new Client();

            $response = $http->post(url('/oauth/token'), [
                'form_params' => $params,
                'headers' => [
                    'Guard' => 'client',
                ]
            ]);

            $response = json_decode($response->getBody()->getContents());
            $response->id = auth('client')->user()->id;
            return json_encode($response);
        }

        return response()->json([
            'message' => 'The given data was invalid.',
            'errors'  => [
                'password' => [
                    'The selected email or password is invalid.'
                ]
            ]
        ], 422);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $user = Auth::guard('client-api')->user();

        if (null !== $user) {
            $access_token = $user->token();

            if (null !== $access_token) {
                DB::table('oauth_refresh_tokens')
                    ->where('access_token_id', $access_token->id)
                    ->update(['revoked' => true]);

                $access_token->revoke();

                return response()->json([], 200);
            }

            $request->session()->invalidate();

            return response()->json([], 200);
        }

        return response()->json([], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function refreshToken(Request $request)
    {
        $http = new Client();

        try {
            $response = $http->post(url('/oauth/token'), [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $request->refresh_token,
                    'client_id'     => $this->client->id,
                    'client_secret' => $this->client->secret,
                    'scope' => '',
                ],
            ]);

            $code = json_decode((string) $response->getBody()->getContents(), true);

            if (!$code) {
                return response()->json([], 401);
            }
        } catch (\Exception $e) {
            return response()->json([], 401);
        }

        return response()->json($code, 200);
    }
}
