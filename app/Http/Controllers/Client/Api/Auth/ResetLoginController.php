<?php

namespace App\Http\Controllers\Client\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResetLoginController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->user = auth('client-api')->user();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request): JsonResponse
    {
        if (filter_var($request->login, FILTER_VALIDATE_EMAIL)) {
            $validator = Validator::make($request->all(), [
                'login' => 'required|email|unique:App\Entities\User,email,'.$this->user->id.',id',
            ]);
            $field = 'email';
        }else{
            $validator = Validator::make($request->all(), [
                'login' => 'required|unique:App\Entities\User,phone,'.$this->user->id.',id',
            ]);
            $field = 'phone';
        }

        if ($validator->fails()) {
            return $this->response('The given data was invalid.', $validator->getMessageBag()->toArray(), 422);
        }

        $this->user->update([
            $field => $request->login
        ]);

        return $this->response('success', []);

    }

    /**
     * @param $items
     * @param int $status
     * @return JsonResponse
     */
    protected function response(string $message, array $errors, int $status = 200):JsonResponse
    {
        return response()->json([
            'message' => $message,
            'errors' => $errors,
        ], $status);
    }
}
