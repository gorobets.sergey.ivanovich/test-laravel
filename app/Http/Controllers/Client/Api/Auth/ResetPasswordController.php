<?php

namespace App\Http\Controllers\Client\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Client\ResetPassword;
use App\Http\Requests\Client\UpdatePassword;
use App\Http\Requests\Client\VerifyPhoneTokenRequest;
use App\Jobs\EsputnikEventJob;
use App\Jobs\UserEventJob;
use App\Repository\Client\UserRepository;
use App\Services\User\ResetPassword\UserResetPasswordService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    use AuthenticatesUsers;

    /**
     * @var UserRepository
     */
    public $repository;

    /**
     * @var UserResetPasswordService
     */
    public $service;

    private $user;


    public function __construct()
    {
        $this->repository = new UserRepository();
        $this->service = new UserResetPasswordService();
        $this->user = null;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(Request $request):JsonResponse
    {
        $this->validate($request,[
            'field' => 'required',
        ]);

        if (filter_var($request->field, FILTER_VALIDATE_EMAIL)) {
            $type = 'email';
            $user = $this->repository->searchFromEmail($request->field);
            $this->validate($request,[
                'field' => 'required|email|exists:App\Entities\User,email',
            ]);
            UserEventJob::dispatch('reset_password', $request->field, null, null, $this->service->generateResetToken($user));
        }else{
            $type = 'phone';
            $user = $this->repository->searchFromPhone($request->field);
            $this->validate($request,[
                'field' => 'required|exists:App\Entities\User,phone',
            ]);
            EsputnikEventJob::dispatch('resetPasswordPhone', null, $user->phone, null, $this->service->generateResetPhoneToken($user));
        }

        if(!$user){
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors'  => [
                    'email' => [
                        'The selected email is invalid.'
                    ]
                ]
            ], 422);
        }

        return response()->json([
            'type' => $type
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function verify(Request $request)
    {
        return view('auth.reset',[
            'token' => $request->token
        ]);
    }

    /**
     * @param ResetPassword $request
     */
    public function update(ResetPassword $request)
    {
        $this->user = $this->repository->searchFromToken($request->reset_token);

        if($this->user && $this->user->id == $request->id){
            $this->user->reset_token = null;
            $this->passwordUpdate($request->password);

            if(!$request->phone){
                UserEventJob::dispatch('update_password', $this->user->email);
            }
        }

        return $this->response('success');
    }

    /**
     * @param UpdatePassword $request
     * @return JsonResponse
     */
    public function updatePassword(UpdatePassword $request): JsonResponse
    {
        $this->user = auth('client-api')->user();
        $this->passwordUpdate($request->password);

        return $this->response('success');
    }

    public function verifyPhoneCode(VerifyPhoneTokenRequest $request)
    {
        $user = $this->repository->searchFromToken($request->phone_code);

        if($user && Carbon::now()->diffInSeconds($user->phone_expired_at, false) > 0){
            return response()->json([
                'url' => route('user.verify.password', ['token' => $this->service->generateResetToken($user)])
            ], 200);
        }

        return response()->json([
            'token time expired'
        ], 404);
    }

    private function passwordUpdate(string $password)
    {
        $this->user->password=Hash::make($password);
        $this->user->update();
    }

    /**
     * @param $items
     * @param int $status
     * @return JsonResponse
     */
    protected function response($items, int $status = 200):JsonResponse
    {
        return response()->json([
            'data' => $items
        ], $status);
    }
}
