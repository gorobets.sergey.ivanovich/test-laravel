<?php

namespace App\Http\Controllers\Client\Api\Auth;

use App\Http\Controllers\Controller;
use App\Services\User\ResetPassword\UserResetPasswordService;
use GuzzleHttp\Client;
use Laravel\Passport\Client as PassportClient;
use Laravel\Socialite\Facades\Socialite;
use Exception;

class SocialAuthController extends Controller
{

    protected $client;

    /**
     * AdminLoginController constructor.
     */
    public function __construct()
    {
        $client = new PassportClient;
        $this->client = $client->where('name', 'Client')->where('password_client', 1)->first();
        if (!$this->client) {
            abort(400, __('system.passport'));
        }
    }

    /**
     * @param $service
     * @return mixed
     */
    public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect('/');
    }

    /**
     * @param $service
     * @return false|\Illuminate\Http\JsonResponse|string
     */
    public function handleProviderCallback($service)
    {
        try {
            $user = Socialite::driver($service)->user();
            $create = [];

            if($service === 'facebook'){
                $create['name'] = $user->getName();
                $create['email'] = $user->getEmail() ?? $user->getId();
                $create['facebook_id'] = $user->getId() ?? null;
                $create['google_id'] = null;
                $create['apple_id'] = null;
                $create['password_str'] = random_bytes(15);
                $create['password'] = bcrypt( bin2hex($create['password_str']));
            }else if($service === 'google'){
                $create['name'] = $user->name;
                $create['email'] = $user->email;
                $create['google_id'] = $user->id;
                $create['facebook_id'] = null;
                $create['apple_id'] = null;
                $create['password_str'] = random_bytes(15);
                $create['password'] = bcrypt( bin2hex($create['password_str']));
            }else if($service === 'apple'){
                $create['name'] = $user->name;
                $create['email'] = $user->email;
                $create['apple_id'] = $user->id;
                $create['facebook_id'] = null;
                $create['google_id'] = null;
                $create['password_str'] = random_bytes(15);
                $create['password'] = bcrypt( bin2hex($create['password_str']));
            }

            $createdUser = (new UserResetPasswordService())->create($service,$create);

            if(is_null($createdUser)){
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'params' => [
                        'service' => $service,
                        $service.'_id' => $create[$service.'_id'],
                        'email' => $create['email']
                    ]
                ], 422);
            }

            $params = [
                'grant_type'    => 'password',
                'client_id'     => $this->client->id,
                'client_secret' => $this->client->secret,
                'username'      => trim($createdUser->email),
                'password'      => trim($createdUser->password_str),
                'scope'         => '*',
            ];

            $http = new Client();

            $response = $http->post(url('/oauth/token'), [
                'form_params' => $params,
                'headers' => [
                    'Guard' => 'client',
                ]
            ]);

            $response = json_decode($response->getBody()->getContents());
            $response->id = auth('client')->user()->id;
            $response = json_encode($response);

            return $response;
        } catch (Exception $e) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'params' => []
            ], 422);
        }
    }
}
