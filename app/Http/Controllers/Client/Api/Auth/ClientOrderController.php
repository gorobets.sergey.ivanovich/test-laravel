<?php

namespace App\Http\Controllers\Client\Api\Auth;

use App\Http\Controllers\Client\Api\ClientApiController;
use App\Http\Requests\Order\Client\Create;
use App\Services\User\Order\OrderService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientOrderController extends ClientApiController
{
    /**
     * @var OrderService
     */
    private $service;

    public function __construct(OrderService $service)
    {
        $this->service = $service;

        parent::__construct();
    }

    /**
     * @param Request $request
     * @return JsonResource
     */
    public function index(Request $request): JsonResource
    {
        return $this->service->index();
    }

    /**
     * @param Create $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function create(Create $request): JsonResponse
    {
        return $this->service->create($request->validated());
    }
}
