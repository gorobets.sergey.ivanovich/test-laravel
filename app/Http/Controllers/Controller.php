<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function setLocale(Request $request, $locale)
    {
        $urlPrevious = explode('/', url()->previous());

        if(!empty($urlPrevious[3]) && in_array($urlPrevious[3], config('app.locales'))){
            $urlPrevious[3] = $locale;
        }else{
            $urlPrevious[3] = $locale;
        }

        $url = implode('/', $urlPrevious);

        return redirect($url);

    }
}
