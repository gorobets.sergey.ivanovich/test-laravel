<?php

namespace App\Http\Resources;

use App\Models\Entity\OrdersCustomerTypesEntity;
use App\Models\Entity\OrdersPaymentTypesEntity;
use App\Models\OrdersEntityClientContact;
use App\Models\OrdersNewClientContact;
use App\Models\OrdersRegularClientContact;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class AuthClientOrderHistoryResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'date' => $this->created_at->format('Y-m-d H:i'),
            'status' => trans('orders.status')[$this->status]
        ];
    }

}
