<?php

namespace App\Http\Resources;

use App\Models\Entity\OrdersCustomerTypesEntity;
use App\Models\Entity\OrdersPaymentTypesEntity;
use App\Models\OrdersEntityClientContact;
use App\Models\OrdersNewClientContact;
use App\Models\OrdersRegularClientContact;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class AuthClientOrderResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'contacts' => $this->getCustomerData($this->client_type_id),
            'deliveryAddress' => $this->deliveryAddress->address,
            'paymentDetails' => $this->payment_type_id === OrdersPaymentTypesEntity::PAYMENT_IN_PARTS
                ? $this->paymentDetails->details
                : null,
            'products' => array_map(function ($item){
                return Arr::only($item,[
                    'article', 'title', 'price', 'price_sale', 'sale_with_price', 'count'
                ]);
            }, $this->products->toArray()),
            'totalPrice' => $this->total_price,
            'order_status' => trans('orders.status')[$this->status]
        ];
    }

    /**
     * @param int $type
     * @return void
     */
    private function getCustomerData(int $type): array
    {
        $contacts = [];
        switch ($type){
            case OrdersCustomerTypesEntity::NEW_CUSTOMER:
                $contacts = Arr::only($this->newCustomerContacts->toArray(),(new OrdersNewClientContact())->getFillable());
                break;
            case OrdersCustomerTypesEntity::REGULAR_CUSTOMER:
                $contacts = Arr::only($this->regularCustomerContacts->toArray(),(new OrdersRegularClientContact())->getFillable());
                break;
            case OrdersCustomerTypesEntity::ENTITY_CUSTOMER:
                $contacts = Arr::only($this->entityCustomerContacts->toArray(),(new OrdersEntityClientContact())->getFillable());
                break;
        }

        return $contacts;
    }
}
