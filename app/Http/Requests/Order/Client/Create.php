<?php

namespace App\Http\Requests\Order\Client;

use App\Models\Entity\OrdersCustomerTypesEntity;
use App\Models\Entity\OrdersDeliveryTypesEntity;
use App\Models\Entity\OrdersPaymentTypesEntity;
use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id' => 'required|exists:App\Entities\City,id',
            'client_type_id' => 'required|exists:App\Entities\OrdersCustomerTypes,id',
            'delivery_type_id' => 'required|exists:App\Entities\OrdersDeliveryTypes,id',
            'payment_type_id' => 'required|exists:App\Entities\OrdersPaymentTypes,id',
            'comment' => 'nullable|string',
            'name' => 'required_if:client_type_id,'.OrdersCustomerTypesEntity::NEW_CUSTOMER.','.OrdersCustomerTypesEntity::ENTITY_CUSTOMER,
            'phone' => 'required_if:client_type_id,'.OrdersCustomerTypesEntity::NEW_CUSTOMER.','.OrdersCustomerTypesEntity::ENTITY_CUSTOMER,
            'email' => 'nullable|email',
            'entity_title' => 'required_if:client_type_id,'.OrdersCustomerTypesEntity::ENTITY_CUSTOMER.'|string|min:5|max:255',
            'entity_inn' =>  'required_if:client_type_id,'.OrdersCustomerTypesEntity::ENTITY_CUSTOMER.'|string|min:5|max:255',
            'entity_address' => 'required_if:client_type_id,'.OrdersCustomerTypesEntity::ENTITY_CUSTOMER.'|string|min:5|max:255',
            'delivery_address' => 'required_if:delivery_type_id,'.OrdersDeliveryTypesEntity::BY_COURIER.','.OrdersDeliveryTypesEntity::MEEST.'|json',
            'payment_details' => 'required_if:payment_type_id,'.OrdersPaymentTypesEntity::PAYMENT_IN_PARTS.'|json',
        ];
    }

    public function response(array $errors)
    {
        return response()->json($errors, 400);
    }
}
