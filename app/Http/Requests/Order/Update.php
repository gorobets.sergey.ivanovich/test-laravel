<?php

namespace App\Http\Requests\Order;

class Update extends Create
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['id'] = 'required|exists:App\Entities\Orders,id';
        $rules['status'] = 'required|in:0,1,2';
        $rules['card_number'] = 'nullable|max:9999';

        return $rules;
    }

    public function response(array $errors)
    {
        return response()->json($errors, 400);
    }
}
