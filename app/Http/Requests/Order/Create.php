<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|min:2|max:255',
            'pre_order' => 'nullable|in:0,1',
            'is_custom' => 'nullable|in:0,1',
            'product_case_id' => 'nullable|exists:App\Entities\Product,id',
            'email' => 'nullable|string|min:2|max:255|email',
            'phone' => 'required|string|min:2|max:20',
            'promo' => 'nullable|string|min:2|max:255',
            'comment' => 'nullable|string|min:2|max:2500',
            'payment_type_id' => 'nullable|exists:App\Entities\PaymentType,id',
            'period' => 'nullable|integer|min:2|max:30',
            'card_number' => 'required_if:payment_type_id,2|max:9999',
            'ipn' => 'nullable|string|min:2|max:255',
            'address' => 'nullable|string|min:2|max:255',
            'passport' => 'nullable|string|min:2|max:255',
            'date_birthday' => 'nullable|date',
        ];
    }

    public function response(array $errors)
    {
        return response()->json($errors, 400);
    }
}
