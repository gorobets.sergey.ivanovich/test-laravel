<?php

namespace App\Http\Requests\UserCart;

use Illuminate\Foundation\Http\FormRequest;

class UserCartAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cart' => 'required|array',
            'cart.*' => 'required|array|size:2',
            'cart.*.id' => 'required|exists:App\Entities\Product,id',
            'cart.*.count' => 'required|integer|min:1|max:100'
        ];
    }
}
