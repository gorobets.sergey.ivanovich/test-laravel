<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use App\Models\SlugLog;

abstract class AbstractSlugLogObserver
{
    /**
     * Handle the customer "updated" event.
     *
     * @param Model  $model
     * @return void
     */
    public function updated($model)
    {
        $changes = $model->getChanges();
        if (array_key_exists('slug', $changes)) {
            // is not exist row with this model and id
            if (
                null !== $model->getOriginal('slug') &&
                !$model->slugLogs()->where('slug', $model->getOriginal('slug'))->first()
            ) {
                SlugLog::where('target_type', $model->getMorphClass())
                    ->whereIn('slug', [$model->getOriginal('slug'), $changes['slug']])
                    ->delete();
                // set new log with old slug
                $model->slugLogs()->create(['slug' => $model->getOriginal('slug')]);
            }
        }
    }

    /**
     * Handle the customer "deleted" event.
     *
     * @param Model  $model
     * @return void
     */
    public function deleted($model)
    {
        // Cascade delete
        // SlugLog::where('target_type', $model->getMorphClass())
        //    ->where('target_id', $model->getKey())
        //    ->delete();
        $model->slugLogs()->delete();
    }
}
