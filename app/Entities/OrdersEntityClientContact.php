<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class
 * @package App\Entities
 *
 * @ORM\Entity()
 * @ORM\Table(name="orders_entity_client_contacts", indexes={
 *         @ORM\Index(name="idx_orderEntityClientId", columns={"client_order_id"}),
 *     })
 */
class OrdersEntityClientContact
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="OrdersClient")
     * @ORM\JoinColumn(name="client_order_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $client_order_id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $entity_title;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $entity_inn;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $entity_address;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
}
