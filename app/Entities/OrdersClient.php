<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class
 * @package App\Entities
 *
 * @ORM\Entity()
 * @ORM\Table(name="orders_client", indexes={
 *         @ORM\Index(name="idx_orderClientTypeId", columns={"client_type_id"}),
 *         @ORM\Index(name="idx_orderClientCityId", columns={"city_id"}),
 *         @ORM\Index(name="idx_orderDeliveryTypeId", columns={"delivery_type_id"}),
 *         @ORM\Index(name="idx_orderDeliveryPaymentTypeId", columns={"payment_type_id"})
 *     })
 */
class OrdersClient
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $city_id;

    /**
     * @ORM\ManyToOne(targetEntity="OrdersCustomerTypes")
     * @ORM\JoinColumn(name="client_type_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $client_type_id;

    /**
     * @ORM\ManyToOne(targetEntity="OrdersDeliveryTypes")
     * @ORM\JoinColumn(name="delivery_type_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $delivery_type_id;

    /**
     * @ORM\ManyToOne(targetEntity="OrdersDeliveryTypes")
     * @ORM\JoinColumn(name="payment_type_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $payment_type_id;

    /**
     * @var integer
     * @ORM\Column(type="smallint", options={"default"= 1})
     */
    private $status;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $comment;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $details;

    /**
     * @var integer
     * @ORM\Column(type="integer", options={"default"= 1})
     */
    private $total_price;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
}
