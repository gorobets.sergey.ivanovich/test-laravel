<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class
 * @package App\Entities
 *
 * @ORM\Entity()
 * @ORM\Table(name="orders")
 */
class Orders
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $comment;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $promo;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $article;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $property;

    /**
     * @var integer
     * @ORM\Column(type="smallint", options={"default"= 0})
     */
    private $status;

    /**
     * @var float
     * @ORM\Column(type="float", precision=2, scale=10, nullable=true)
     */
    private $price;

    /**
     * @var float
     * @ORM\Column(type="float", precision=2, scale=10, nullable=true)
     */
    private $price_sale;

    /**
     * @var float
     * @ORM\Column(type="float", precision=2, scale=10, nullable=true)
     */
    private $sale_with_price;

    /**
     * @var string
     * @ORM\Column(type="text", length=1500, nullable=true)
     */
    private $action;

    /**
     * @ORM\ManyToOne(targetEntity="PaymentType")
     * @ORM\JoinColumn(name="payment_type_id", referencedColumnName="id", onDelete="CASCADE", nullable=true, columnDefinition="INT NOT NULL DEFAULT 1")
     */
    private $payment_type_id;

    /**
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $product_id;

    /**
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="product_case_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $product_case_id;

    /**
     * @var integer
     * @ORM\Column(type="smallint", options={"default"= 0})
     */
    private $period;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $card_number;

    /**
     * @var integer
     * @ORM\Column(type="smallint", options={"default"= 0})
     */
    private $order_checked;

    /**
     * @ORM\Column(name="get_order_info_at", type="datetime", nullable=true)
     */
    private $get_order_info_at;

    /**
     * @ORM\Column(name="get_order_info_status", type="string", nullable=true)
     */
    private $get_order_info_status;

    /**
     * @ORM\Column(name="order_cancel_status", type="string", nullable=true)
     */
    private $order_cancel_status;

    /**
     * @ORM\Column(name="order_guarantee_confirm_status", type="string", nullable=true)
     */
    private $order_guarantee_confirm_status;

    /**
     * @ORM\Column(name="order_bank_id", type="string", nullable=true)
     */
    private $order_bank_id;

    /**
     * @var float
     * @ORM\Column(type="float", precision=2, scale=10, nullable=true)
     */
    private $price_with_percent;

    /**
     * @var float
     * @ORM\Column(type="integer", nullable=true)
     */
    private $percent;

    /**
     * @ORM\Column(name="alfa_status_callback", type="string", nullable=true)
     */
    private $alfa_status_callback;

    /**
     * @var integer
     * @ORM\Column(type="smallint", options={"default"= 0})
     */
    private $pre_order;

    /**
     * @var integer
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $is_custom;

    /**
     * @ORM\Column(name="ipn", type="string", nullable=true)
     */
    private $ipn;

    /**
     * @ORM\Column(name="passport", type="string", nullable=true)
     */
    private $passport;

    /**
     * @ORM\Column(name="address", type="string", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(name="date_birthday", type="datetime", nullable=true)
     */
    private $date_birthday;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
}
