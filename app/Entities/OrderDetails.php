<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class
 * @package App\Entities
 *
 * @ORM\Entity()
 * @ORM\Table(name="order_details", indexes={
 *         @ORM\Index(name="idx_orderDetailClientId", columns={"client_order_id"}),
 *         @ORM\Index(name="idx_orderDetailProductId", columns={"product_id"})
 *     })
 */
class OrderDetails
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="OrdersClient")
     * @ORM\JoinColumn(name="client_order_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $client_order_id;

    /**
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $product_id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $article;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $title;

    /**
     * @var float
     * @ORM\Column(type="float", precision=2, scale=10, nullable=false)
     */
    private $price;

    /**
     * @var float
     * @ORM\Column(type="float", precision=2, scale=10, nullable=false)
     */
    private $price_sale;

    /**
     * @var float
     * @ORM\Column(type="float", precision=2, scale=10, nullable=false)
     */
    private $sale_with_price;

    /**
     * @var integer
     * @ORM\Column(type="smallint", nullable=false)
     */
    private $count;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updated_at;
}
