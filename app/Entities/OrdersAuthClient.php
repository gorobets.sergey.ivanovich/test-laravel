<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class
 * @package App\Entities
 *
 * @ORM\Entity()
 * @ORM\Table(name="orders_auth_client", indexes={
 *         @ORM\Index(name="idx_orderAuthClientId", columns={"client_order_id"}),
 *         @ORM\Index(name="idx_orderAuthUserClientId", columns={"user_id"}),
 *     })
 */
class OrdersAuthClient
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="OrdersClient")
     * @ORM\JoinColumn(name="client_order_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $client_order_id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $user_id;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
}
