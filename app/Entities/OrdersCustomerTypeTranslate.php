<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class
 * @package App\Entities
 *
 * @ORM\Entity()
 * @ORM\Table(name="orders_customer_type_translate", indexes={
 *         @ORM\Index(name="idx_orderCustomerTypeId", columns={"order_customer_type_id"})
 *     })
 */
class OrdersCustomerTypeTranslate
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="OrdersCustomerTypes")
     * @ORM\JoinColumn(name="order_customer_type_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $order_customer_type_id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $lang;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $title;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
}
