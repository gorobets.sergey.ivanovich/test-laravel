<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class
 * @package App\Entities
 *
 * @ORM\Entity()
 * @ORM\Table(name="orders_payment_type_translates", indexes={
 *         @ORM\Index(name="idx_orderPaymentTypeTranslateId", columns={"order_payment_type_id"})
 *     })
 */
class OrdersPaymentTypeTranslates
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $lang;

    /**
     * @ORM\ManyToOne(targetEntity="OrdersPaymentTypes")
     * @ORM\JoinColumn(name="order_payment_type_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $order_payment_type_id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $title;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
}
