<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class
 * @package App\Entities
 *
 * @ORM\Entity()
 * @ORM\Table(name="orders_payment_details", indexes={
 *         @ORM\Index(name="idx_orderDeliveryAddressClientId", columns={"client_order_id"}),
 *         @ORM\Index(name="idx_orderPaymentTypeId", columns={"payment_type_id"})
 *     })
 */
class OrderPaymentDetails
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="OrdersClient")
     * @ORM\JoinColumn(name="client_order_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $client_order_id;

    /**
     * @ORM\ManyToOne(targetEntity="OrdersPaymentTypes")
     * @ORM\JoinColumn(name="payment_type_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $payment_type_id;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $details;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
}
