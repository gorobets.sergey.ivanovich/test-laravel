<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class
 * @package App\Entities
 *
 * @ORM\Entity()
 * @ORM\Table(name="user_cart_product", indexes={
 *         @ORM\Index(name="idx_userCartItemId", columns={"cart_id"}),
 *         @ORM\Index(name="idx_userCarProducttId", columns={"product_id"}),
 *     })
 */
class UserCartProduct
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserCart")
     * @ORM\JoinColumn(name="cart_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $cart_id;

    /**
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $product_id;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $count;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
}
