<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class
 * @package App\Entities
 *
 * @ORM\Entity()
 * @ORM\Table(name="user_addresses", indexes={
 *         @ORM\Index(name="idx_userAddresActive", columns={"is_active"}),
 *         @ORM\Index(name="idx_userAddressDefault", columns={"is_default"}),
 *         @ORM\Index(name="idx_userAddress", columns={"user_id"})
 *     })
 */
class UserAddresses
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $user_id;

    /**
     * @var string
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $street;

    /**
     * @var string
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $apartments;

    /**
     * @var string
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $full_name;

    /**
     * @var string
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $phone;

    /**
     * @var integer
     * @ORM\Column(type="boolean", options={"default"= 0})
     */
    private $is_active;

    /**
     * @var integer
     * @ORM\Column(type="boolean", options={"default"= 0})
     */
    private $is_default;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
}
