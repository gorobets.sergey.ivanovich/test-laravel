<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class User
 * @package App\Entities
 *
 * @ORM\Entity()
 * @ORM\Table(name="users", uniqueConstraints={
 *         @ORM\UniqueConstraint(name="unique_users_email", columns={"email"})
 *     }
 * )
 */
class User
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $surname;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $facebook_id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $google_id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $remember_token;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $reset_token;

    /**
     * @var string
     * @ORM\Column(name="email_verified_at", type="datetime", nullable=true)
     */
    private $email_verified_at;

    /**
     * @ORM\Column(name="phone_expired_at", type="datetime", nullable=true)
     */
    private $phone_expired_at;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
}
