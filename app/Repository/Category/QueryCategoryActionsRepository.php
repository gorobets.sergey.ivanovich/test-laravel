<?php

namespace App\Repository\Category;

use App\Models\Action;
use App\Models\Product;
use App\Repository\BaseQuery;
use Illuminate\Support\Facades\DB;

class QueryCategoryActionsRepository extends BaseQuery
{
    public $category;
    public $data;

    public function __construct($category)
    {
        $this->data = new ActionFiltersRepository;
        $this->category = $category;
    }

    public function concatData()
    {
        return 'CONCAT_WS("||", actions.id, action_translate.title, actions.slug, actions.percent, UNIX_TIMESTAMP(actions.close_at)-UNIX_TIMESTAMP(), p.id, actions.icon)';
    }

    public function concatDataCashOs()
    {
        return 'CONCAT_WS("||", p.slug, property_values_translate.value)';
    }

    public function concatDataCashColor()
    {
        return 'CONCAT_WS("||", p.slug, property_values_translate.value, property_values_translate.color_code_0, property_values_translate.color_code_1)';
    }

    public function prepareSql()
    {
        return DB::table('products as p')
            ->distinct()
            ->select(
                'p.id as id',
                'p.is_active as is_active',
                'p.is_archive as is_archive'
            );
    }

    public function actions($productIds = [], $locale = null)
    {
        DB::statement('SET GLOBAL group_concat_max_len = 1000000');

        $locale = !$locale ? app()->getLocale() : $locale;
        $sql = $this->prepareSql();
        $sql = $this->addSelectForCategory($sql, $locale)
            ->join('actions_products as ap', 'ap.product_id', 'p.id')
            ->where(function ($q){
                $q->whereNull('p.deleted_at')
                    ->where('p.category_id', $this->category)
                    ->where('p.is_active', Product::IS_ACTIVE)
                    ->where('p.price', '>',  Product::PRICE_ZERO);
            })
            ->get();

        return $productIds ? $this->data->itemFilterToArray($sql, $productIds) : $this->data->itemToArray($sql);
    }

    public function countWithActions($pIds, $actions)
    {
        $sql = $this->prepareSql();
        $sql = $this->addSelectForCategoryFilterActions($sql, $actions)
            ->join('actions_products as ap', 'ap.product_id', 'p.id')
            ->whereNull('p.deleted_at')
            ->where('p.price', '>',  Product::PRICE_ZERO);

        if ($pIds) {
            $sql = $sql->whereIn('p.id', $pIds);
        }

        $ids = [];
        $actions = [];

        foreach ($sql->get() as $item) {
            if( ($item->action_1 || $item->action_2) && $item->is_active && !$item->is_archive){
                $ids[] = $item->id;
            }
        }

        return $ids;
    }

    public function actionsProduct($id)
    {
        $sql = DB::table('products as p')->select('p.id as id');
        $sql = $this->addSelectForCategory($sql)
            ->where('p.id', $id)
            ->get();

        return $this->data->actionsProductToArray($sql);
    }

}
