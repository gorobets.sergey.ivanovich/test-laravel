<?php

namespace App\Repository\Category;

use App\Repository\BaseQuery;
use Illuminate\Support\Facades\DB;

class QueryCategoryRepository extends BaseQuery
{
    public function categories($ids)
    {
        $sql = DB::table('сategory_translate')
            ->select('category_id', 'title', 'c.slug')
            ->join('categories as c', 'c.id', 'сategory_translate.category_id')
            ->where(function ($q) use($ids){
                $q->whereIn('c.slug', $ids)
                    ->where('lang', app()->getLocale());
            })
            ->get();

        return $sql;
    }

    /**
     * @param $categoryId
     * @return array
     */
    public function getFiltersSql($categoryId):array
    {
        DB::statement('SET GLOBAL group_concat_max_len = 1000000');

        return DB::table('parameters')
            ->select(
                DB::raw('COUNT(parameters.value_id) as count_value'),
                DB::raw('GROUP_CONCAT(parameters.product_id) as products'),
                'p.slug as v_slug',
                'p.value as v_title',
                'properties.slug',
                'parameters.property_id',
                'pt.title as property_title',
                'p.image as image',
                'properties.order_filter as order_filter',
                'properties.is_hide as is_hide',
                'p.order as order',
                'parameters.value_id')
            ->join('properties', function ($join) {
                $join->on('properties.id', '=', 'parameters.property_id')
                    ->where('properties.is_filter', 1)
                    ->where('properties.is_active', 1);
            })
            ->join('property_translate as pt', function ($join) {
                $join->on('pt.property_id', '=', 'parameters.property_id')
                    ->where('pt.lang', app()->getLocale());
            })
            ->join('products as pr', function ($join) {
                $join->on('pr.id', '=', 'parameters.product_id')
                    ->where('pr.is_active', '>', 0)
                    ->where('pr.is_archive', 0);
            })
            ->join('property_values_translate as p', 'p.id', 'parameters.value_id')
            ->where('parameters.category_id', $categoryId)
            ->where('is_price', 1)
            ->orderBy('order_filter')
            ->groupBy('value_id')
            ->get()
            ->groupBy('property_id')
            ->all();
    }

    /**
     * @param array $values
     * @param array $pIds
     * @return array
     */
    public function propertyValuesSlug(array $values, array $pIds):array
    {
        return DB::table('property_values_translate')
            ->select('slug', 'value')
            ->where(function($q) use($values, $pIds){
                $q->whereIn('slug', $values)
                    ->whereIn('property_id', $pIds)
                    ->where('lang', app()->getLocale());
            })
            ->pluck('value', 'slug')
            ->all();
    }

    /**
     * @param array $productIds
     * @return array
     */
    public function filtersForCategoryProducts(array $productIds):array
    {
        DB::statement('SET GLOBAL group_concat_max_len = 1000000');

        return DB::table('parameters')
            ->select(
                DB::raw('COUNT(parameters.value_id) as count_value'),
                DB::raw('GROUP_CONCAT(parameters.product_id) as products'),
                'p.slug as v_slug',
                'p.value as v_title',
                'properties.slug',
                'parameters.property_id',
                'pt.title as property_title',
                'p.image as image',
                'properties.order_filter as order_filter',
                'properties.is_hide as is_hide',
                'parameters.value_id')
            ->join('properties', 'properties.id', 'parameters.property_id')
            ->join('property_translate as pt', function ($join) {
                $join->on('pt.property_id', '=', 'parameters.property_id')
                    ->where('pt.lang', app()->getLocale());
            })
            ->join('property_values_translate as p', 'p.id', 'parameters.value_id')
            ->whereIn('parameters.product_id', $productIds)
            ->where('is_price', 1)
            ->where('properties.is_filter', 1)
            ->orderBy('order_filter')
            ->groupBy('value_id')
            ->havingRaw('COUNT(count_value)')
            ->get()
            ->groupBy('property_id')
            ->all();
    }

    /**
     * @param $categoryId
     * @return array
     */
    public function getFiltersSqlSitemap($categoryId):array
    {
        return DB::table('parameters')
            ->select(
                DB::raw('COUNT(parameters.value_id) as count_value'),
                'p.slug as v_slug',
                'p.value as v_title',
                'properties.slug',
                'parameters.property_id',
                'pt.title as property_title',
                'p.image as image',
                'properties.order_filter as order_filter',
                'properties.is_hide as is_hide',
                'p.order as order',
                'parameters.category_id as category_id',
                'parameters.value_id')
            ->join('properties', function ($join) {
                $join->on('properties.id', '=', 'parameters.property_id')
                    ->where('properties.is_filter', 1)
                    ->where('properties.is_active', 1);
            })
            ->join('property_translate as pt', function ($join) {
                $join->on('pt.property_id', '=', 'parameters.property_id')
                    ->where('pt.lang', app()->getLocale());
            })
            ->join('products as pr', function ($join) {
                $join->on('pr.id', '=', 'parameters.product_id')
                    ->where('pr.is_active', '>', 0)
                    ->where('pr.is_archive', 0);
            })
            ->join('property_values_translate as p', function ($join) {
                $join->on('p.id', '=', 'parameters.value_id')
                    ->whereRaw('LENGTH(p.seo_title) > 0');
            })
            ->where('parameters.category_id', $categoryId)
            ->where('is_price', 1)
            ->orderBy('order_filter')
            ->groupBy('value_id')
            ->havingRaw('COUNT(count_value)')
            ->get()
            ->groupBy('property_id')
            ->all();
    }

    public  function categoryProducts(int $category): array
    {
        return DB::table('products')
            ->where('category_id', $category)
            ->where('is_active', '>', 0)
            ->where('price', '>', 0)
            ->whereNull('is_archive')
            ->pluck('id')->all();
    }

    public function getProductsIdsWithFiltersCash($property, $values, $category): array
    {
        return DB::table('category_property as c')
            ->select('pd.id as id')
            ->join('properties as p', function ($join) use($property){
                $join->on('c.property_id', '=', 'p.id')
                    ->where('p.slug', $property);
            })
            ->join('property_values_translate as pvt', function ($join) use($values){
                $join->on('p.id', '=', 'pvt.property_id')
                    ->whereIn('pvt.slug', explode('-or-', $values))
                    ->where('pvt.lang', 'ru');
            })
            ->join('parameters as pr_s', function ($join) use($values){
                $join->on('pr_s.value_id', '=', 'pvt.id');
            })
            ->join('products as pd', function ($join) use($values){
                $join->on('pd.id', '=', 'pr_s..product_id')
                    ->where('pd.is_active', 1)
                    ->where('pd.is_archive', 0);
            })
            ->where('c.category_id', $category)
            ->pluck('id')->all();
    }
}
