<?php

namespace App\Repository\Product;

use App\Models\Action;
use App\Models\ActionProduct;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductGallery;
use App\Models\ProductParameters;
use App\Models\ProductRelation;
use App\Repository\BaseQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class QueryProductRepository extends BaseQuery
{
    /**
     * @var QueryProductRepository
     */
    private $data;
    private $limit;
    private $size;

    const OFFSET = 1;
    const LIMIT = 1;

    /**
     * QueryProductRepository constructor.
     */
    public function __construct()
    {
        $this->data = new ProductRepository;
        $this->limit = config('limits.product_main_actions');
        $this->size = explode('x', env('IMAGE_SIZE_PRODUCT_CART'))[0];
    }

    public function concatData()
    {
        return 'CONCAT_WS("||",actions.percent, DATE_FORMAT(start_at, "%d.%m.%Y"), DATE_FORMAT(close_at, "%d.%m.%Y"), actions.id, actions.icon)';
    }

    public function prepareSqlMinimumSelect(): \Illuminate\Database\Query\Builder
    {
        return DB::table('products as p')
            ->distinct()
            ->select(
                'p.id as id',
                'p.is_availability as is_availability',
                'p.price as price',
                'p.sale_price as sale_price',
                'p.is_action as is_action',
                'p.pre_order as pre_order',
                'p.is_archive as is_archive',
                'p.is_expected as is_expected',
                'p.is_hit as is_hit',
                'p.is_new as is_new',
                'p.deadline_start_at as deadline_start_at',
                'p.deadline_finish_at as deadline_finish_at',
                DB::raw('CONCAT(' . $this->size . ', "_", p.image) as image'),
                DB::raw('CEIL(IF(p.price <> p.sale_price, p.sale_price, IF(p.sale_with_price > 0, p.sale_with_price, p.sale_price))) as price_result'),
                'p.price as price',
                'p.sale_with_price as sale_with_price'
            );
    }

    public function prepareSqlWithoutAction()
    {
        return DB::table('products as p')
            ->distinct()
            ->select(
                'p.id as id',
                'p.category_id as category',
                'p.slug as slug',
                'p.sale_price as sale_price',
                'p.is_availability as is_availability',
                DB::raw('CONCAT(' . $this->size . ', "_", p.image) as image'),
                DB::raw('IF(p.price <> p.sale_price, p.sale_price, IF(p.sale_with_price > 0, p.sale_with_price, p.sale_price)) as price_result'),
                'pt.image_alt as image_alt',
                'pt.title as title',
                'pt.short_property_list as short_property_list',
                'pt.lang as lang',
                'p.price as price',
                'p.sale_with_price as sale_with_price',
                'p.is_action as is_action',
                'p.is_hit as is_hit',
                'p.is_new as is_new',
                'p.is_archive as is_archive',
                'p.is_expected as is_expected',
                'p.is_custom as is_custom',
                'p.is_case as is_case',
                'p.is_custom as is_custom',
                'p.pre_order as pre_order',
                'p.deadline_finish_at as deadline_finish_at',
                'p.deadline_start_at as deadline_start_at',
                DB::raw('NOW() as time')
            );
    }

    public function addSelectImage($sql)
    {
        return $sql->addSelect([
            'img_1' => ProductGallery::select(DB::raw('CONCAT(id, "||'.$this->size.'_'.'", image, "||", weight)'))
                ->whereColumn('product_id', 'p.id')
                ->where('weight', '<', Product::GALLERY_WEIGHT_START)
                ->orderBy('weight')
                ->limit(self::LIMIT),
            'img_2' => ProductGallery::select(DB::raw('CONCAT(id, "||'.$this->size.'_'.'", image, "||", weight)'))
                ->whereColumn('product_id', 'p.id')
                ->where('weight', '<', Product::GALLERY_WEIGHT_START)
                ->orderBy('weight')
                ->limit(self::LIMIT)->offset(self::OFFSET)
        ]);
    }

    /**
     * @return array
     */
    public function getProductActions()
    {
        $sql = $this->prepareSqlWithoutAction();
        $sql = $this->addSelectImage($sql);
        $sql = $this->addSelectForCategory($sql)
            ->join('product_translate as pt', 'pt.product_id', 'p.id')
            ->join('actions_products as ap', 'ap.product_id', 'p.id')
            ->whereNull('p.deleted_at')
            ->where(function ($q){
                $q->where('p.is_action', Product::IS_ACTION)
                    ->where('p.is_active', Product::IS_ACTIVE)
                    ->where('p.is_availability', Product::IS_AVAILABILITY)
                    ->where('p.is_archive', '!=', Product::IS_ARCHIVE)
                    ->where('p.price', '>',  Product::PRICE_MINIMUM)
                    ->where('pt.lang', app()->getLocale());
            })
            ->limit($this->limit)
            ->get();

        return $this->data->itemToArray($sql);
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    private function prepareSql()
    {
        $sql = $this->prepareSqlWithoutAction();
        return $this->addSelectImage($sql)
            ->join('product_translate as pt', 'pt.product_id', 'p.id');
    }

    /**
     * @param $itemsIgnore
     * @param $idsIgnore
     * @return array
     */
    public function getProductNew()
    {
        $sql = $this->prepareSql()->where(function ($item){
            $item->whereNull('p.deleted_at')
                ->where('p.is_archive', '!=', Product::IS_ARCHIVE)
                ->where('p.is_active', Product::IS_ACTIVE)
                ->where('p.is_availability', Product::IS_AVAILABILITY)
                ->where('p.is_new', Product::IS_NEW)
                ->where('p.main_tabs', Product::MAIN_TAB)
                ->where('p.price', '>',  Product::PRICE_MINIMUM)
                ->where('pt.lang', app()->getLocale());
        })
            ->orderBy('main_tabs_order_new')
            ->limit($this->limit)
            ->get();

        return $this->data->itemToArray($sql);
    }

    /**
     * @param $itemsIgnore
     * @param $idsIgnore
     * @return array
     */
    public function getProductHit()
    {
        $sql = $this->prepareSql()->where(function ($item) {
            $item->whereNull('p.deleted_at')
                ->where('p.is_active', Product::IS_ACTIVE)
                ->where('p.is_archive', '!=', Product::IS_ARCHIVE)
                ->where('p.is_availability', Product::IS_AVAILABILITY)
                ->where('p.is_hit', Product::IS_HIT)
                ->where('p.main_tabs', Product::MAIN_TAB)
                ->where('p.price', '>',  Product::PRICE_MINIMUM)
                ->where('pt.lang', app()->getLocale());
        })
            ->orderBy('main_tabs_order_hit')
            ->limit($this->limit)
            ->get();

        return $this->data->itemToArray($sql);
    }

    /**
     * @param $itemsIgnore
     * @param $idsIgnore
     * @return array
     */
    public function getProductDeadline()
    {
        $sql = $this->prepareSql()->where(function ($item) {
            $item->whereNull('p.deleted_at')
                ->where('p.is_active', Product::IS_ACTIVE)
                ->where('p.is_archive', '!=', Product::IS_ARCHIVE)
                ->where('p.is_availability', Product::IS_AVAILABILITY)
                ->where('p.price', '>',  Product::PRICE_MINIMUM)
                ->whereNotNull('p.deadline_start_at')
                ->whereBetween(DB::raw('TIMESTAMP(NOW())'), [
                    DB::raw('TIMESTAMP(deadline_start_at)'),
                    DB::raw('TIMESTAMP(deadline_finish_at)')
                ])
                ->where('pt.lang', app()->getLocale());
        })
            ->orderByDesc('p.id')
            ->limit($this->limit)
            ->get();

        return $this->data->itemToArray($sql);
    }

    /**
     * @param $itemsIgnore
     * @param $idsIgnore
     * @return array
     */
    public function getProductPreOrder()
    {
        $sql = $this->prepareSql()->where(function ($item) {
            $item->whereNull('p.deleted_at')
                ->where('p.is_active', Product::IS_ACTIVE)
                ->where('p.is_archive', '!=', Product::IS_ARCHIVE)
                ->where('p.pre_order', Product::PRE_ORDER)
                ->where('p.price', '>',  Product::PRICE_MINIMUM)
                ->where('pt.lang', app()->getLocale());
        })
            ->orderBy('main_tabs_order_hit')
            ->limit($this->limit)
            ->get();

        return $this->data->itemToArray($sql);
    }

    /**
     * @param $itemsIgnore
     * @param $idsIgnore
     * @return array
     */
    public function getProductCustom(): array
    {
        $sql = $this->prepareSql()->where(function ($item) {
            $item->whereNull('p.deleted_at')
                ->where('p.is_active', Product::IS_ACTIVE)
                ->where('p.is_archive', '!=', Product::IS_ARCHIVE)
                ->where('p.is_custom', Product::IS_CUSTOM)
                ->where('pt.lang', app()->getLocale());
        })
            ->orderByDesc('p.id')
            ->limit($this->limit)
            ->get();

        return $this->data->itemToArray($sql);
    }

    /**
     * @param $ids
     * @return array
     */
    public function getProductWatched($ids): array
    {
        $sql = $this->prepareSqlWithoutAction();
        $sql = $this->addSelectImage($sql);
        $sql = $this->addSelectForCategory($sql)
            ->join('product_translate as pt', function ($join){
                $join->on('p.id', 'pt.product_id')
                    ->where('pt.lang', app()->getLocale());
            })
            ->whereNull('p.deleted_at')
            ->whereIn('p.id', $ids)
            ->where('p.is_active', Product::IS_ACTIVE)
            ->where('p.price', '>',  Product::PRICE_MINIMUM)
            ->where('pt.lang', app()->getLocale())
            ->whereNull('p.is_custom')
            ->whereNull('p.is_case')
            ->get();

        return $this->data->itemToArray($sql);
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    public function prepareSqlForCategory(): \Illuminate\Database\Query\Builder
    {
        return $this->prepareSqlWithoutAction()
            ->addSelect(
                'p.article as article',
                'p.is_clone as is_clone',
                'p.clone_id as clone_id',
                'p.is_os as is_os',
                'p.is_clone as is_clone',
                'p.clone_article as clone_article',
                'p.os_title as os_title',
                'p.is_expected as is_expected',
                'p.is_archive as is_archive',
                'p.category_id as category',
                'p.pre_order as pre_order',
                'p.is_custom as is_custom',
                'p.deadline_start_at as deadline_start_at',
                'p.deadline_finish_at as deadline_finish_at',
                'p.deadline_start_at as deadline_start_at',
                'p.visit_month_at as visit_month_at',
                'p.old_month_popular as old_month_popular',
                'p.new_month_popular as new_month_popular',
                'p.created_at as created_at',
                'p.weight as weight',
                DB::raw('IF(MONTH(p.visit_month_at) != MONTH(NOW()) AND DAYOFMONTH(NOW()) < 11,p.old_month_popular, p.new_month_popular) as popular'),
                DB::raw('IF((p.sale_with_price AND p.deadline_start_at AND TIMESTAMP(p.deadline_start_at) < TIMESTAMP(NOW())) OR (p.sale_with_price AND p.deadline_start_at IS NULL) ,p.sale_with_price,IF(p.sale_price,p.sale_price,p.price)) as price_order'),
                'c.is_os as is_category_os'
            );
    }

    public function productsForCategory($pIds, $category)
    {
        $sql = $this->prepareSqlForCategory();
        $sql = $this->addSelectImage($sql);
        $sql = $this->addSelectOs($sql, $category);
        $sql = $this->addSelectOsRelation($sql, $category);
        $sql = $this->addSelectForCategory($sql)
            ->join('product_translate as pt', 'pt.product_id', 'p.id')
            ->join('categories as c', 'c.id', 'p.category_id')
            ->whereIn('p.id', $pIds ?? [])
            ->where('pt.lang', app()->getLocale())
            ->orderBy('is_availability', 'desc')
            ->orderBy('is_expected', 'asc')
            ->orderBy('is_archive', 'asc');

        return $sql;
    }

    public function productsActualData($pIds = []): \Illuminate\Support\Collection
    {
        return $this->prepareSqlMinimumSelect()->whereIn('p.id', $pIds)->get();
    }

    public function productsYetForCategory($pIds, $category)
    {
        $sql = $this->prepareSqlForCategory();
        $sql = $this->addSelectImage($sql);
        $sql = $this->addSelectOs($sql, $category);
        $sql = $this->addSelectOsRelation($sql, $category);
        $sql = $this->addSelectForCategory($sql)
            ->join('product_translate as pt', function ($join){
                $join->on('p.id', 'pt.product_id')
                    ->where('pt.lang', app()->getLocale());
            })
            ->join('categories as c', 'c.id', 'p.category_id')
            ->where(function ($q) use($pIds){
                $q->whereIn('p.id', $pIds)
                    ->where('p.is_active', '>', 0)
                    ->where('p.price', '>', 0)
                    ->where('p.is_archive', '!=', Product::IS_ARCHIVE);
            })
            ->orderBy('is_availability', 'desc')
            ->orderBy('is_expected', 'asc');

        return $sql;
    }

    public function actionProducts($id)
    {
        $products = DB::table('actions_products')
            ->where('action_id', $id)
            ->pluck('product_id')->all();

        $idsP = DB::table('products')
            ->whereIn('id', $products)
            ->where('is_archive', '!=', Product::IS_ARCHIVE)
            ->pluck('id')->all();

        return $this->data->productsIaActionArray(
            $this->getProductWatched($idsP), $id
        );
    }

    public function getProductBuyData($product)
    {
        $sql = DB::table('products as p')
            ->distinct()
            ->select(
                'p.id as id',
                'p.category_id as category',
                'p.slug as slug',
                'p.sale_price as sale_price',
                'p.is_availability as is_availability',
                DB::raw('CONCAT(' . $this->size . ', "_", p.image) as image'),
                'pt.image_alt as image_alt',
                'pt.title as title',
                'p.price as price',
                'p.sale_with_price as sale_with_price',
                'p.is_action as is_action',
                'p.is_archive as is_archive',
                'p.is_expected as is_expected',
                'p.is_hit as is_hit',
                'p.is_new as is_new',
                'p.pre_order as pre_order',
                'p.is_custom as is_custom',
                'p.deadline_start_at as deadline_start_at',
                'p.deadline_finish_at as deadline_finish_at',
                DB::raw('NOW() as time')
            );
        $sql = $sql->addSelect([
            'img_1' => ProductGallery::select(DB::raw('CONCAT(id, "||'.$this->size.'_'.'", image, "||", weight)'))
                ->whereColumn('product_id', 'p.id')
                ->where('weight', '<', 2)
                ->orderBy('weight')
                ->limit(self::LIMIT),
            'img_2' => ProductGallery::select(DB::raw('CONCAT(id, "||'.$this->size.'_'.'", image, "||", weight)'))
                ->whereColumn('product_id', 'p.id')
                ->where('weight', '<', 2)
                ->orderBy('weight')
                ->limit(self::LIMIT)->offset(self::OFFSET)
        ]);
        $sql = $sql->addSelect([
            'action_1' => ActionProduct::select([
                'data' => Action::select(DB::raw('CONCAT(percent, "||", DATE_FORMAT(start_at, "%d.%m.%Y"), "||", DATE_FORMAT(close_at, "%d.%m.%Y"), "||", action_id)'))
                    ->whereColumn('id', 'actions_products.action_id')->limit(self::LIMIT)
                    ->where('is_active', 1)
                    ->whereBetween(DB::raw('NOW()'), [
                        DB::raw('DATE(start_at)'),
                        DB::raw('DATE(close_at)')
                    ])
                    ->orderBy('close_at')
            ])
                ->whereColumn('product_id', 'p.id')
                ->limit(self::LIMIT),
            'action_2' => ActionProduct::select([
                'data' => Action::select(DB::raw('CONCAT(percent, "||", DATE_FORMAT(start_at, "%d.%m.%Y"), "||", DATE_FORMAT(close_at, "%d.%m.%Y"), "||", action_id)'))
                    ->whereColumn('id', 'actions_products.action_id')->limit(self::LIMIT)
                    ->where('is_active', 1)
                    ->whereBetween(DB::raw('NOW()'), [
                        DB::raw('DATE(start_at)'),
                        DB::raw('DATE(close_at)')
                    ])
                    ->orderBy('close_at')
            ])
                ->whereColumn('product_id', 'p.id')
                ->limit(self::LIMIT)->offset(self::OFFSET)
        ])
            ->join('product_translate as pt', 'pt.product_id', 'p.id')
            ->whereNull('p.deleted_at')
            ->where('p.id', $product->id)
            ->where('pt.lang', app()->getLocale())
            ->limit($this->limit)
            ->get();

        return $this->data->itemToArray($sql, true);
    }

    public function queryPercentProduct($productId)
    {
        return '(SELECT a.percent from actions_products as ap INNER JOIN actions AS a ON a.id = ap.action_id WHERE ap.product_id='.$productId.')';
    }

    public function queryForOrder($order)
    {
        $sql = DB::table('orders as o')->distinct()
            ->select(
                'p.id as product_id',
                'pt.title as title',
                'p.price as price',
                'pt.description as description',
                'p.sale_price as sale_price',
                'p.sale_with_price as sale_with_price',
                'p.is_archive as is_archive',
                'p.is_action as is_action',
                'o.comment as comment',
                'o.name as fName',
                'o.phone as phone',
                'o.price_with_percent as price_with_percent',
                'o.email as email',
                DB::raw('IF(p.is_action, '.$this->queryPercentProduct($order->product_id).', 0) as action_percent')
            )
            ->join('products as p','o.product_id', 'p.id')
            ->join('product_translate as pt', function ($join){
                $join->on('p.id', 'pt.product_id')
                    ->where('pt.lang', 'uk');
            })
            ->where('o.id', $order->id)
            ->get();

        return $this->data->itemsOrderToArray($sql);
    }

    /**
     * @param $id
     * @return array
     */
    public function queryForComparison($id): array
    {
        $sql = DB::table('products as p')
            ->distinct()
            ->select(
                'p.id as id',
                'p.slug as slug',
                'p.deadline_start_at as deadline_start_at',
                'p.deadline_finish_at as deadline_finish_at',
                'p.article as article',
                'p.category_id as category',
                'p.is_custom as is_custom',
                'p.is_case as is_case',
                'c.slug as category_slug',
                'pt.title as title'
            )->addSelect([
            'image' => ProductGallery::select(DB::raw('CONCAT(id, "||'.$this->size.'_'.'", image, "||", weight)'))
                ->whereColumn('product_id', 'p.id')
                ->where('weight', '<', 2)
                ->orderBy('weight')
                ->limit(self::LIMIT)
            ])->join('product_translate as pt', 'pt.product_id', 'p.id')
            ->join('categories as c', 'c.id', 'p.category_id')
            ->whereNull('p.deleted_at')
            ->where('p.id', $id)
            ->where('pt.lang', app()->getLocale())
            ->whereNull('p.is_custom')
            ->whereNull('p.is_case')
            ->first();

        return $this->data->addComparisonArray($sql);
    }

    public function sessionComparison()
    {
        return Session::get('comparison') ?? [
                'products' => [],
                'url' => route('index'),
            ];
    }

    public function comparisonFromUrl($articles)
    {
        $sql = DB::table('products as p')
            ->distinct()
            ->select(
                'p.id as id',
                'p.slug as slug',
                'p.article as article',
                'p.deadline_start_at as deadline_start_at',
                'p.deadline_finish_at as deadline_finish_at',
                'p.category_id as category',
                'c.slug as category_slug',
                'pt.title as title'
            )->addSelect([
                'image' => ProductGallery::select(DB::raw('CONCAT(id, "||'.$this->size.'_'.'", image, "||", weight)'))
                    ->whereColumn('product_id', 'p.id')
                    ->where('weight', '<', 2)
                    ->orderBy('weight')
                    ->limit(self::LIMIT)
            ])->join('product_translate as pt', 'pt.product_id', 'p.id')
            ->join('categories as c', 'c.id', 'p.category_id')
            ->whereNull('p.deleted_at')
            ->whereNull('p.is_custom')
            ->whereIn('p.article', $articles)
            ->where('pt.lang', app()->getLocale())
            ->get();

        $data = [];

        foreach ($sql as $item) {
            $data[$item->article] = $this->data->addComparisonArray($item);
        }

        return $data;
    }

    public function productParameters($ids)
    {
        return DB::table('parameters as p')
            ->select('p.product_id as product_id',
                'p.property_id as property_id',
                'p.value_id as value_id',
                'pt.title as property_title',
                'pvt1.value as value_title',
                'pr.order_cart as order_cart'
            )
            ->join('properties as pr',function ($join){
                $join->on('pr.id', '=', 'p.property_id')
                    ->where('pr.is_cart', '>', 0);
            })
            ->join('property_translate as pt', function ($join){
                $join->on('pt.property_id', 'p.property_id')
                    ->where('pt.lang', app()->getLocale());
            })
            ->join('property_values_translate as pvt', function ($join){
                $join->on('pvt.id', 'p.value_id')
                    ->join('property_values_translate as pvt1', function ($j){
                        $j->on('pvt1.partner', 'pvt.partner')
                            ->where('pvt1.lang', app()->getLocale());
                });
            })
            ->whereIn('product_id', $ids)
            ->orderBy('pr.order_cart')
            ->get();
    }

    public function sessionProductCount($ids)
    {
        return DB::table('products as p')
            ->distinct()
            ->select(
                'p.id as id',
                'p.category_id as category'
            )->whereIn('p.id', $ids)
            ->get();
    }

    public function getShortProperties($ids)
    {
        $partners = DB::table('parameters as par')
            ->select(
                'pvt1.value as value',
                'par.product_id as id',
                'p.order_short as order_short'
            )
            ->join('properties as p',function ($join){
                $join->on('p.id', '=', 'par.property_id')->where('p.is_short', 1)->where('p.is_cart', 1);
            })
            ->join('property_values_translate as pvt',function ($join) {
                $join->on('pvt.id', '=', 'par.value_id')
                    ->join('property_values_translate as pvt1',function ($join) {
                    $join->on('pvt.partner', '=', 'pvt1.partner')->where('pvt1.lang','uk');
                });
            })
            ->whereIn('par.product_id', $ids)->orderBy('p.order_short')->get();

        return $this->data->getShortProperties($partners);
    }

    /**
     * @param $ids
     * @param $lang
     * @return Collection
     */
    public function getShortPropertiesSeedSql($ids, $lang): Collection
    {
        return DB::table('parameters as par')
            ->select(
                'pvt1.value as value',
                'pvt1.id as value_id',
                'par.product_id as id',
                'p.order_short as order_short',
                'pt.title as title',
                'pr.article as article'
            )
            ->join('properties as p',function ($join){
                $join->on('p.id', '=', 'par.property_id')
                    ->where(function ($q){
                        $q->where('p.is_short', 1);
                    });
            })
            ->join('property_values_translate as pvt',function ($join) use($lang){
                $join->on('pvt.id', '=', 'par.value_id')
                    ->join('property_values_translate as pvt1',function ($join) use($lang){
                        $join->on('pvt.partner', '=', 'pvt1.partner')->where('pvt1.lang',$lang);
                    });
            })
            ->join('product_translate as pt',function ($join) use($lang){
                $join->on('pt.product_id', '=', 'par.product_id')
                    ->where(function ($q) use($lang){
                        $q->where('pt.lang', $lang);
                    });
            })
            ->join('products as pr',function ($join) use($lang){
                $join->on('pr.id', '=', 'par.product_id');
            })
            ->whereIn('par.product_id', $ids)
            ->orderBy('p.order_short')
            ->get();
    }

    public function getShortPropertiesSeed($ids, $lang = 'uk')
    {
        return $this->data->getShortPropertiesSeed($this->getShortPropertiesSeedSql($ids,$lang));
    }

    public function addSelectOs($sql, $category)
    {
        $property_id = (new Category())->getPropertyOs($category);

        return $sql->addSelect([
            'os_self' => ProductParameters::select(DB::raw('GROUP_CONCAT(property_values_translate.value)'))
                ->where(function ($q) use($property_id){
                    $q->whereColumn('parameters.product_id', 'p.id')
                        ->where('parameters.property_id', $property_id);
                })->join('property_values_translate', function ($join){
                    $join->on('property_values_translate.id', '=', 'parameters.value_id');
                })
        ]);
    }

    public function addSelectSearchOs($sql)
    {
        return $sql->addSelect([
            'os_self' => ProductParameters::select(DB::raw('GROUP_CONCAT(property_values_translate.value)'))
                ->where(function ($q){
                    $q->whereColumn('parameters.product_id', 'p.id')
                        ->where(DB::raw('parameters.property_id=IF(p.category_id=1,47,IF(p.category_id=2,277,IF(p.category_id=6,351,0)))'));
                })->join('property_values_translate', function ($join){
                    $join->on('property_values_translate.id', '=', 'parameters.value_id');
                })
        ]);
    }

    public function addSelectOsRelation($sql, $category)
    {
        $property_id = (new Category())->getPropertyOs($category);

        return $sql->addSelect([
            'os_relation_main' => ProductRelation::select(DB::raw('CONCAT(p2.slug, "||", GROUP_CONCAT(property_values_translate.value))'))
                ->where(function ($q) use($property_id){
                    $q->whereColumn('product_realations.liaison_product_id', 'p.id')
                        ->where('product_realations.property_id', $property_id);
                })->join('parameters', function ($join) use($property_id){
                    $join->on('parameters.product_id', '=', 'product_realations.product_id')
                        ->where('parameters.property_id', $property_id);
                })->join('property_values_translate', function ($join){
                    $join->on('property_values_translate.id', '=', 'parameters.value_id');
                })->join('products as p2', function ($join){
                    $join->on('p2.id', '=', 'product_realations.product_id');
                }),
            'os_relation_second' => ProductRelation::select(DB::raw('CONCAT(p2.slug, "||", GROUP_CONCAT(property_values_translate.value))'))
                ->where(function ($q) use($property_id){
                    $q->whereColumn('product_realations.product_id', 'p.id')
                        ->where('product_realations.property_id', $property_id);
                })->join('parameters', function ($join) use($property_id){
                    $join->on('parameters.product_id', '=', 'product_realations.liaison_product_id')
                        ->where('parameters.property_id', $property_id);
                })->join('property_values_translate', function ($join){
                    $join->on('property_values_translate.id', '=', 'parameters.value_id');
                })->join('products as p2', function ($join){
                    $join->on('p2.id', '=', 'product_realations.liaison_product_id');
                }),
        ]);
    }

    public function addSelectOsRelationSearch($sql)
    {
        return $sql->addSelect([
            'os_relation_main' => ProductRelation::select(DB::raw('CONCAT(p2.slug, "||", GROUP_CONCAT(property_values_translate.value))'))
                ->where(function ($q){
                    $q->whereColumn('product_realations.liaison_product_id', 'p.id')
                        ->where(DB::raw('product_realations.property_id=IF(p.category_id=1,47,IF(p.category_id=2,277,IF(p.category_id=6,351,0)))'));
                })->join('parameters', function ($join) {
                    $join->on('parameters.product_id', '=', 'product_realations.product_id')
                        ->where(DB::raw('parameters.property_id=IF(parameters.category_id=1,47,IF(parameters.category_id=2,277,IF(parameters.category_id=6,351,0)))'));
                })->join('property_values_translate', function ($join){
                    $join->on('property_values_translate.id', '=', 'parameters.value_id');
                })->join('products as p2', function ($join){
                    $join->on('p2.id', '=', 'product_realations.product_id');
                }),
            'os_relation_second' => ProductRelation::select(DB::raw('CONCAT(p2.slug, "||", GROUP_CONCAT(property_values_translate.value))'))
                ->where(function ($q){
                    $q->whereColumn('product_realations.product_id', 'p.id')
                        ->where(DB::raw('product_realations.property_id=IF(p.category_id=1,47,IF(p.category_id=2,277,IF(p.category_id=6,351,0)))'));
                })->join('parameters', function ($join){
                    $join->on('parameters.product_id', '=', 'product_realations.liaison_product_id')
                        ->where(DB::raw('parameters.property_id=IF(parameters.category_id=1,47,IF(parameters.category_id=2,277,IF(parameters.category_id=6,351,0)))'));
                })->join('property_values_translate', function ($join){
                    $join->on('property_values_translate.id', '=', 'parameters.value_id');
                })->join('products as p2', function ($join){
                    $join->on('p2.id', '=', 'product_realations.liaison_product_id');
                }),
        ]);
    }

    public function productsForCategoryIndex($category, $price)
    {
        $sql = $this->prepareSqlForCategory();
        $sql = $this->addSelectImage($sql);
        $sql = $this->addSelectOs($sql, $category);
        $sql = $this->addSelectOsRelation($sql, $category);
        $sql = $this->addSelectForCategory($sql)
            ->join('product_translate as pt', 'pt.product_id', 'p.id')
            ->join('categories as c', 'c.id', 'p.category_id')
            ->where(function ($q) use($category){
                $q->where('p.category_id', $category)
                    ->where('p.is_active', '>', 0)
                    ->where('p.price', '>', 0)
                    ->where('p.is_archive', '!=', Product::IS_ARCHIVE);
            });

        if($price){
            $sql = $sql->whereRaw('IF(p.sale_with_price,p.sale_with_price,IF(p.sale_price,p.sale_price,p.price)) BETWEEN ? AND ?', $price);
        }

        $sql = $sql->where('pt.lang', app()->getLocale())
            ->orderBy('is_availability', 'desc')
            ->orderBy('is_expected', 'asc');

        return $sql;
    }

    public function productsForSearchIndex($ids)
    {
        $sql = $this->prepareSqlForCategory();
        $sql = $this->addSelectImage($sql);
        $sql = $this->addSelectSearchOs($sql);
        $sql = $this->addSelectOsRelationSearch($sql);
        $sql = $this->addSelectForCategory($sql)
            ->join('product_translate as pt', 'pt.product_id', 'p.id')
            ->join('categories as c', 'c.id', 'p.category_id')
            ->where(function ($q) use($ids){
                $q->whereIn('p.id', $ids)
                    ->where('p.is_active', '>', 0)
                    ->where('p.price', '>', 0);
            });

        $sql = $sql->where('pt.lang', app()->getLocale())
            ->orderBy('is_availability', 'desc')
            ->orderBy('is_expected', 'asc')
            ->orderBy('is_archive', 'asc');

        return $sql;
    }

    public function productsForCategoryCash($category)
    {
        $sql = $this->prepareSqlForCategory();
        $sql = $this->addSelectImage($sql);
        $sql = $this->addSelectOs($sql, $category);
        $sql = $this->addSelectOsRelation($sql, $category);
        return $this->addSelectForCategory($sql)
            ->join('product_translate as pt', 'pt.product_id', 'p.id')
            ->join('categories as c', 'c.id', 'p.category_id')
            ->where(function ($q) use($category){
                $q->where('p.category_id', $category)
                    ->where('p.is_active', '>', 0)
                    ->where('p.price', '>', 0)
                    ->where('p.is_archive', '!=', Product::IS_ARCHIVE)
                    ->where('pt.lang', app()->getLocale());
            })->orderBy('is_availability', 'desc');
    }

    public function productsForCategorySearchCash($category)
    {
        $sql = $this->prepareSqlForCategory();
        $sql = $this->addSelectImage($sql);
        $sql = $this->addSelectOs($sql, $category);
        $sql = $this->addSelectOsRelation($sql, $category);
        return $this->addSelectForCategory($sql)
            ->join('product_translate as pt', 'pt.product_id', 'p.id')
            ->join('categories as c', 'c.id', 'p.category_id')
            ->where(function ($q) use($category){
                $q->where('p.category_id', $category)
                    ->where('p.is_active', '>', 0)
                    ->where('p.price', '>', 0)
                    ->where('pt.lang', app()->getLocale());
            })->orderBy('is_availability', 'desc');
    }

    /**
     * @param $price
     * @param $category
     * @return array
     */
    public function productsForCategoryFiltesPrice($price, $category, $ids):array
    {
        $sql = DB::table('products as p')
            ->distinct()
            ->select(
                'p.id as id'
            )
            ->where(function ($q) use($category){
                $q->where('p.category_id', $category)
                    ->where('p.is_active', '>', 0)
                    ->where('p.price', '>', 0)
                    ->where('p.is_archive', '!=', Product::IS_ARCHIVE);
            })->where(function ($q) use($price){
                $q->whereBetween('p.price', $price)
                    ->orWhereBetween('p.sale_price',$price)
                    ->orWhere(function ($query) use($price){
                        $query->whereNotNull('p.sale_with_price')
                            ->orWhereBetween('p.sale_with_price',$price);
                    });
            });

        if($ids){
            $sql = $sql->whereIn('p.id', $ids);
        }

        return $sql->pluck('p.id')->all();
    }

    /**
     * @param array $ids
     * @param bool $addCondition
     * @return array|Collection
     */
    public function getProductGallery(array $ids, bool $addCondition = false)
    {
        return $ids ? DB::table('product_galleries')
            ->distinct()
            ->select(
                'id', 'product_id', 'image', 'weight'
            )
            ->whereIn('product_id', $ids)
            ->when($addCondition, function ($q){
                $q->where('weight', '<', 2);
            })
            ->orderBy('weight')
            ->get() : [];
    }

    /**
     * @return Collection
     */
    public function landingProducts(): Collection
    {
        return DB::table('products as p')
            ->distinct()
            ->select(
                'p.id as id',
                'p.slug as slug',
                'p.category_id as category',
                'p.article as article',
                'p.sale_price as sale_price',
                'p.is_availability as is_availability',
                DB::raw('CONCAT(' . $this->size . ', "_", p.image) as image'),
                'pt.image_alt as image_alt',
                'pt.title as title',
                'p.price as price',
                'p.sale_with_price as sale_with_price',
                'p.is_os as is_os',
                'p.is_action as is_action',
                'p.is_clone as is_clone',
                'p.is_new as is_new',
                'p.is_hit as is_hit',
                'p.is_archive as is_archive',
                'p.is_expected as is_expected',
                'p.is_custom as is_custom',
                'p.clone_id as clone_id',
                'p.os_title as os_title',
                'p.clone_article as clone_article',
                'p.pre_order as pre_order',
                'p.deadline_start_at as deadline_start_at',
                'p.deadline_finish_at as deadline_finish_at',
                'c.is_os as is_category_os'
            )
            ->join('product_translate as pt', 'pt.product_id', 'p.id')
            ->join('categories as c', 'c.id', 'p.category_id')
            ->whereNull('p.deleted_at')
            ->whereIn('p.article', config('landing'))
            ->where('pt.lang', app()->getLocale())
            ->get();
    }

    /**
     * @param array $ids
     * @return Collection
     */
    public function landingProductsNew(array $ids): Collection
    {
        return DB::table('products as p')
            ->distinct()
            ->select(
                'p.id as id',
                'p.slug as slug',
                'p.article as article',
                'p.category_id as category',
                'p.sale_price as sale_price',
                'p.is_availability as is_availability',
                DB::raw('CONCAT(' . $this->size . ', "_", p.image) as image'),
                'pt.image_alt as image_alt',
                'pt.title as title',
                'p.price as price',
                'p.sale_with_price as sale_with_price',
                'p.is_os as is_os',
                'p.is_action as is_action',
                'p.is_new as is_new',
                'p.is_hit as is_hit',
                'p.is_archive as is_archive',
                'p.is_expected as is_expected',
                'p.is_clone as is_clone',
                'p.is_custom as is_custom',
                'p.clone_id as clone_id',
                'p.os_title as os_title',
                'p.clone_article as clone_article',
                'p.pre_order as pre_order',
                'p.deadline_start_at as deadline_start_at',
                'p.deadline_finish_at as deadline_finish_at',
                'c.is_os as is_category_os'
            )
            ->join('product_translate as pt', 'pt.product_id', 'p.id')
            ->join('categories as c', 'c.id', 'p.category_id')
            ->whereNull('p.deleted_at')
            ->where('pt.lang', app()->getLocale())
            ->whereIn('p.id', $ids)
            ->get();
    }

    public function parametersItems(array $ids): Collection
    {
       return DB::table('parameters as par')->distinct()
            ->select('par.product_id as product_id', 'par.value_id as value_id', 'pvt.partner as partner')
            ->join('properties as p', 'p.id', 'par.property_id')
            ->join('property_values_translate as pvt', 'pvt.id', 'par.value_id')
            ->where('p.is_short', '>', '0')
            ->whereIn('par.product_id', $ids)
            ->orderBy('p.order_short')
            ->get();
    }

    public function productParametersValues(array $ids): Collection
    {
        return DB::table('property_values_translate')->distinct()
            ->select('id', 'property_id', 'value', 'image', 'partner')
            ->whereIn('partner', array_unique($ids))
            ->where('lang', app()->getLocale())
            ->get();
    }

    public function prepareSeedForProductsIndex(array $ids = []): Collection
    {
        $sql = DB::table('products as p')->distinct()
            ->select('p.id as product_id',
                'p.article as article',
                'p.is_active as is_active',
                'p.price as price',
                'p.image as image',
                'p.slug as slug',
                'p.is_clone as is_clone',
                'p.is_os as is_os',
                'p.clone_article as clone_article',
                'p.sale_price as sale_price',
                'p.is_availability as is_availability',
                'p.sale_with_price as sale_with_price',
                'p.is_action as is_action',
                'p.is_hit as is_hit',
                'p.is_active as is_active',
                'p.is_expected as is_expected',
                'p.is_archive as is_archive',
                'p.is_case as is_case',
                'p.is_custom as is_custom',
                'p.os_title as os_title',
                'p.deadline_start_at as deadline_start_at',
                'p.deadline_finish_at as deadline_finish_at',
                'p.category_id as category_id',
                'p.is_custom as is_custom',
                'pt.lang as lang',
                'pt.title as title',
                'c.slug as category_slug'
            )
            ->addSelect([
                'action_1' => ActionProduct::select([
                    'data' => Action::select('percent')
                        ->whereColumn('id', 'actions_products.action_id')->limit(1)
                        ->where('is_active', 1)
                        ->whereBetween(DB::raw('NOW()'), [
                            DB::raw('DATE(start_at)'),
                            DB::raw('DATE(close_at)')
                        ])
                        ->orderBy('close_at')
                ])
                    ->whereColumn('product_id', 'p.id')
                    ->limit(1),
                'action_2' => ActionProduct::select([
                    'data' => Action::select('percent')
                        ->whereColumn('id', 'actions_products.action_id')->limit(1)
                        ->where('is_active', 1)
                        ->whereBetween(DB::raw('NOW()'), [
                            DB::raw('DATE(start_at)'),
                            DB::raw('DATE(close_at)')
                        ])
                        ->orderBy('close_at')
                ])
                    ->whereColumn('product_id', 'p.id')
                    ->limit(1)->offset(1)
            ])
            ->join('product_translate as pt', function ($join) {
                $join->on('pt.product_id', '=', 'p.id');
            })
            ->join('categories as c', function ($join) {
                $join->on('c.id', '=', 'p.category_id')
                    ->whereNull('c.is_custom');
            })
            ->leftJoin('actions_products as ap', function ($join) {
                $join->on('ap.product_id', '=', 'p.id');
            })
            ->leftJoin('actions as a', function ($join) {
                $join->on('a.id', '=', 'ap.action_id');
            })
            ->where('p.is_active', 1);

        if($ids){
            $sql = $sql->whereIn('p.id', $ids);
        }

        return $sql->whereNull('p.is_custom')
            ->whereNull('p.is_case')
            ->get();
    }

    public function findProductsFromSearch(Request $request): Collection
    {
        $search = $request->search;
        $category = $request->category_id;
        $is_availability = $request->is_availability;
        $is_active = $request->is_active;

        return DB::table('products as p')
            ->select('p.id')
            ->when($search, function ($q) use($search) {
                $q->join('product_translate as pt', function ($join) {
                    $join->on('pt.product_id', '=', 'p.id');
                })->where(function ($q) use ($search) {
                        $q->where('pt.title', 'like', '%' . $search . '%')
                            ->orWhere('p.article', 'like', '%' . $search . '%');
                    });
            })->when($category, function ($q) use($category){
                $q->where('category_id', $category)->get();
            })->when($is_availability, function ($q) use($is_availability){
                $q->where('is_availability', $is_availability);
            })->when($is_active, function ($q) use($is_active){
                $q->where('is_active', $is_active);
            })
            ->get();
    }
}
