<?php

namespace App\Repository\Product;

use Illuminate\Support\Facades\DB;
use App\Models\Product;
use Illuminate\Database\Query\Builder;

/**
 * Class Slider
 *
 * @package App\Services\Slider
 */
class ProductQuery
{
    /**
     * @param int $id
     * @return Builder
     */
    public function parameters(int $id): Builder
    {
        return DB::table('parameters as par')
            ->select(
                'pt.title as title',
                'pvt.value as value',
                'pvt.color_code_0 as color_code_0',
                'pvt.color_code_1 as color_code_1',
				'pvt.property_id as property_id',
				'pvt.partner as partner',
                'pvt.image as image',
                'pvt.slug as value_slug',
                'par.value_id as value_id',
                'prt.title as product_title',
                'prt.short_description as short_description',
                'prt.image_alt as image_alt',
                'prt.description as description',
                'prt.seo_title as seo_title',
                'prt.seo_description as seo_description',
                'prt.seo_keyword as seo_keyword',
                'prt.seo_content as seo_content',
                'prt.rich_content as rich_content',
                'p.order_short as order_short',
                'p.order_cart as order_cart',
                'p.slug as property_slug',
                'p.is_filter as is_filter',
                DB::raw('IF(p.is_short > 0, true, false) as is_short'),
                DB::raw('IF(p.is_cart > 0, true, false) as is_cart')
            )
            ->join('properties as p',function ($join){
                $join->on('p.id', '=', 'par.property_id');
            })
            ->join('property_translate as pt',function ($join){
                $join->on('pt.property_id', '=', 'par.property_id')
                    ->where('pt.lang', app()->getLocale());
            })
            ->join('property_values_translate as pvt',function ($join) {
                $join->on('pvt.id', '=', 'par.value_id');
            })
            ->join('product_translate as prt',function ($join) {
                $join->on('prt.product_id', '=', 'par.product_id')
                    ->where('prt.lang', app()->getLocale());
            })
            ->where('par.product_id', $id);
    }

    /**
     * @param int $id
     * @return array
     */
    public function parametersOrderShort(int $id): array
    {
        return $this->parametersOrdered($id, 'p.order_short');
    }

    /**
     * @param int $id
     * @return array
     */
    public function parametersOrderCart(int $id): array
    {
        return $this->parametersOrdered($id, 'p.order_cart');
    }

    /**
     * @param int $id
     * @param string $order
     * @return array
     */
    private function parametersOrdered(int $id, string $order):array
    {
        return $this->parameters($id)
            ->orderBy($order)
            ->groupBy('par.value_id')
            ->get()
            ->sortBy('order_cart')
            ->values()
            ->all();
    }

    /**
     * @param Product $product
     * @return array
     */
    public function getParameters(Product $product): array
    {
        $translate = $product->lang(app()->getLocale())->first();

        return [
            'title' => $translate->title,
            'short_description' => $translate->short_description,
            'image_alt' => $translate->image_alt,
            'description' => $translate->description,
            'seo_title' => $translate->seo_title,
            'seo_description' => $translate->seo_description,
            'seo_keyword' => $translate->seo_keyword,
            'seo_content' => $translate->seo_content,
            'rich_content' => $translate->rich_content,
        ];
    }
}
