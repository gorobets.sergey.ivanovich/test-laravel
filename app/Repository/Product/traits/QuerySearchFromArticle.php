<?php

namespace App\Repository\Product\traits;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

trait QuerySearchFromArticle{

    /**
     * @param $article
     * @return array
     */
    public function searchFromTitleArticle($article, array $params = []):array
    {
        $query = $this->prepareQuery($article);
        if(!$params){
            return $query->pluck('product_id')->all();
        }

        $query = $query->where(function ($q) use($params){
            foreach ($params as $param => $value) {
                $q->where($param, $value);
            }
        });

        return $query->pluck('product_id')->all();
    }

    /**
     * @param $article
     * @return Builder
     */
    public function prepareQuery($article): Builder
    {
        return DB::table('product_search')
            ->where(function ($q) use($article){
                $q->where('title', 'LIKE', '%'.$article.'%')
                    ->orWhere('article', $article);
            });
    }
}
