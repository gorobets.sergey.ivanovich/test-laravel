<?php

namespace App\Repository\Product\traits;

use App\Models\Product;
use Illuminate\Support\Facades\DB;

trait QuerySearch{

    /**
     * @param $value
     * @return array
     */
    public function searchFromValues($value):array
    {
        return DB::table('properties_value_search_'.app()->getLocale())->distinct()
            ->where('value', 'LIKE', '%'.$value.'%')
            ->orderBy('priority')
            ->pluck(app()->getLocale() === "ru" ? 'value_id' : 'partner_id')->all();
    }

    /**
     * @param $category
     * @param $params
     * @param array $ids
     * @return array
     */
    public function getProductIdsWithFilters($category, $params, array $ids = []):array
    {
        $data = DB::table('parameters')->distinct()
            ->select('product_id')
            ->join('products', function ($join) {
                $join->on('products.id', '=', 'parameters.product_id')
                    ->where('products.is_archive', '!=', Product::IS_ARCHIVE)
                    ->whereNull('products.is_custom')
                    ->whereNull('products.is_case');
            });

        if($ids){
            $data = $data->whereIn('product_id', $ids);
        }

        return $data->where('property_id', $params[0])
            ->whereIn('value_id', explode("-or-", $params[1]))
            ->get()->pluck('product_id')->all();
    }

    /**
     * @param array $products
     * @param array $valueIds
     * @return array
     */
    public function getValueIdsWithFilterSearch(array $products, array $valueIds, $category = null):array
    {
        if(!$valueIds){
            return  [];
        }

        return DB::table('parameters')->distinct()
            ->select('value_id')
            ->join('products', function ($join) {
                $join->on('products.id', '=', 'parameters.product_id')
                    ->where('products.is_archive', 0)
                    ->whereNull('products.is_custom')
                    ->whereNull('products.is_case');
            })
            ->when($products,function ($q) use($products, $valueIds){
                $q->whereIn('product_id', $products)
                    ->whereIn('value_id', $valueIds);
            })
            ->when(!$products,function ($q) use($valueIds){
                $q->whereIn('value_id', $valueIds);
            })
            ->get()->pluck('value_id')->all();
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function propertySlugData($id)
    {
       return DB::table('properties')
            ->select(
                'properties.id as id','slug', 'is_filter', 'category_id'
            )
            ->join('category_property', 'category_property.property_id', '=', 'properties.id')
            ->where('properties.id', $id)
            ->where('is_active', 1)
            ->first();
    }

    /**
     * @param array $values
     * @return array
     */
    public function partialUrl(array $values):array
    {
        return DB::table('property_values_translate as pvt')->distinct()
            ->select('p.slug as property_slug', 'pvt.slug as value_slug', 'p.is_filter', 'cp.category_id as category_id', 'c.slug')
            ->join('properties as p', 'pvt.property_id', '=', 'p.id')
            ->join('category_property as cp', 'cp.property_id', '=', 'p.id')
            ->join('categories as c', 'c.id', '=', 'cp.category_id')
            ->whereIn('pvt.id', $values)
            ->get()->toArray();

    }

    /**
     * @param array $properties
     * @return array
     */
    public function categorySearchUrl(array $properties):array
    {
        return DB::table('category_property')->distinct()
            ->select('category_id')
            ->whereIn('property_id', $properties)
            ->get()->pluck('category_id')->all();
    }
}
