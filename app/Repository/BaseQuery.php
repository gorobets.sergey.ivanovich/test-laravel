<?php

namespace App\Repository;

use App\Models\Action;
use App\Models\ActionProduct;
use App\Models\ProductRelation;
use Illuminate\Support\Facades\DB;

class BaseQuery
{
    const LIMIT = 1;
    const OFFSET = 1;

    public function betweenTimeStamp()
    {
        return [
            DB::raw('UNIX_TIMESTAMP(start_at)'),
            DB::raw('UNIX_TIMESTAMP(close_at)')
        ];
    }

    public function addSelectForCategory($sql, $locale = null)
    {
        $locale = !$locale ? app()->getLocale() : $locale;
        return $sql->addSelect([
            'action_1' => ActionProduct::select(DB::raw($this->concatData()))
                ->whereColumn('actions_products.product_id', 'p.id')
                ->join('actions', 'actions_products.action_id', 'actions.id')
                ->join('action_translate', function ($join) use($locale){
                    $join->on('action_translate.action_id', '=', 'actions.id')
                        ->where('action_translate.lang', $locale);
                })
                ->where(function ($query){
                    $query->where('actions.is_active', Action::IS_ACTIVE)
                        ->where(function ($q){
                            $q->whereBetween(DB::raw('UNIX_TIMESTAMP()'), $this->betweenTimeStamp());
                        });
                })->orderBy('actions.close_at')
                ->limit(self::LIMIT),
            'action_2' => ActionProduct::select(DB::raw($this->concatData()))
                ->whereColumn('actions_products.product_id', 'p.id')
                ->join('actions', 'actions_products.action_id', 'actions.id')
                ->join('action_translate', function ($join) use($locale){
                    $join->on('action_translate.action_id', '=', 'actions.id')
                        ->where('action_translate.lang', $locale);
                })
                ->where(function ($query){
                    $query->where('actions.is_active', Action::IS_ACTIVE)
                        ->where(function ($q){
                            $q->whereBetween(DB::raw('UNIX_TIMESTAMP()'), $this->betweenTimeStamp());
                        });
                })->orderBy('actions.close_at')
                ->limit(self::LIMIT)->offset(self::OFFSET)
        ]);
    }

    public function addSelectForCategoryCash($sql, $colorId, $osId)
    {
        return $sql->addSelect([
            'os_relation_self' => ProductRelation::select(DB::raw('CONCAT_WS("||", p_1.slug, property_values_translate.value)'))
                ->whereColumn('product_realations.product_id', 'p.id')
                ->join('products as p_1', function ($join) use($osId){
                    $join->on('p_1.id', '=', 'product_realations.liaison_product_id');
                })
                ->join('parameters', function ($join) use($osId){
                    $join->on('parameters.product_id', '=', 'product_realations.liaison_product_id')
                        ->where('parameters.property_id', $osId);
                })
                ->join('property_values_translate', function ($join){
                    $join->on('property_values_translate.id', '=', 'parameters.value_id');
                }),
            'os_relation_liasion' => ProductRelation::select(DB::raw('CONCAT_WS("||", p_1.slug, property_values_translate.value)'))
                ->whereColumn('product_realations.liaison_product_id', 'p.id')
                ->join('products as p_1', function ($join) use($osId){
                    $join->on('p_1.id', '=', 'product_realations.product_id');
                })
                ->join('parameters', function ($join) use($osId){
                    $join->on('parameters.product_id', '=', 'product_realations.product_id')
                        ->where('parameters.property_id', $osId);
                })
                ->join('property_values_translate', function ($join){
                    $join->on('property_values_translate.id', '=', 'parameters.value_id');
                })->limit(self::LIMIT),
            'color_relation_self' => ProductRelation::select(DB::raw('CONCAT_WS("||", p_1.slug, property_values_translate.value, property_values_translate.color_code_0, property_values_translate.color_code_1)'))
                ->whereColumn('product_realations.product_id', 'p.id')
                ->join('products as p_1', function ($join) use($osId){
                    $join->on('p_1.id', '=', 'product_realations.liaison_product_id');
                })
                ->join('parameters', function ($join) use($colorId){
                    $join->on('parameters.product_id', '=', 'product_realations.liaison_product_id')
                        ->where('parameters.property_id', $colorId);
                })
                ->join('property_values_translate', function ($join){
                    $join->on('property_values_translate.id', '=', 'parameters.value_id');
                })->limit(self::LIMIT),
            'color_relation_liasion' => ProductRelation::select(DB::raw('CONCAT_WS("||", p_1.slug, property_values_translate.value, property_values_translate.color_code_0, property_values_translate.color_code_1)'))
                ->whereColumn('product_realations.liaison_product_id', 'p.id')
                ->join('products as p_1', function ($join) use($osId){
                    $join->on('p_1.id', '=', 'product_realations.product_id');
                })
                ->join('parameters', function ($join) use($colorId){
                    $join->on('parameters.product_id', '=', 'product_realations.product_id')
                        ->where('parameters.property_id', $colorId);
                })
                ->join('property_values_translate', function ($join){
                    $join->on('property_values_translate.id', '=', 'parameters.value_id');
                }),
        ]);
    }

    public function addSelectForCategoryFilter($sql)
    {
        return $sql->addSelect([
            'action_1' => ActionProduct::select('actions.slug')
                ->whereColumn('actions_products.product_id', 'p.id')
                ->join('actions', 'actions_products.action_id', 'actions.id')
                ->where(function ($query){
                    $query->where('actions.is_active', Action::IS_ACTIVE)
                        ->where(function ($q){
                            $q->whereBetween(DB::raw('UNIX_TIMESTAMP()'), $this->betweenTimeStamp());
                        });
                })->orderBy('actions.close_at')
                ->limit(self::LIMIT),
            'action_2' => ActionProduct::select('actions.slug')
                ->whereColumn('actions_products.product_id', 'p.id')
                ->join('actions', 'actions_products.action_id', 'actions.id')
                ->where(function ($query){
                    $query->where('actions.is_active', Action::IS_ACTIVE)
                        ->where(function ($q){
                            $q->whereBetween(DB::raw('UNIX_TIMESTAMP()'), $this->betweenTimeStamp());
                        });
                })->orderBy('actions.close_at')
                ->limit(self::LIMIT)->offset(self::OFFSET)
        ]);
    }

    public function addSelectForCategoryFilterActions($sql, $actions)
    {
        return $sql->addSelect([
            'action_1' => ActionProduct::select('actions.slug')
                ->whereColumn('actions_products.product_id', 'p.id')
                ->join('actions', 'actions_products.action_id', 'actions.id')
                ->where(function ($query) use($actions){
                    $query->where('actions.is_active', Action::IS_ACTIVE)
                        ->where(function ($q) use($actions){
                            $q->whereBetween(DB::raw('UNIX_TIMESTAMP()'), $this->betweenTimeStamp())
                                ->whereIn('actions.slug', $actions);
                        });
                })->orderBy('actions.close_at')
                ->limit(self::LIMIT),
            'action_2' => ActionProduct::select('actions.slug')
                ->whereColumn('actions_products.product_id', 'p.id')
                ->join('actions', 'actions_products.action_id', 'actions.id')
                ->where(function ($query) use($actions){
                    $query->where('actions.is_active', Action::IS_ACTIVE)
                        ->where(function ($q) use($actions){
                            $q->whereBetween(DB::raw('UNIX_TIMESTAMP()'), $this->betweenTimeStamp())
                                ->whereIn('actions.slug', $actions);
                        });
                })->orderBy('actions.close_at')
                ->limit(self::LIMIT)->offset(self::OFFSET)
        ]);
    }

}
