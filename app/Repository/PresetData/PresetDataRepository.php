<?php

namespace App\Repository\PresetData;

use Illuminate\Support\Facades\Storage;

class PresetDataRepository
{
    private $service;
    private $data;

    /**
     * PresetDataRepository constructor.
     */
    public function __construct()
    {
        $this->service = new QueryPresetDataRepository();
    }

    /**
     * @return mixed
     */
    public function getMenuData()
    {
        $query = $this->service->getQueryMenu();
        return $this->arrayData($query);
    }

    /**
     * @param $category
     * @return mixed
     */
    public function getCategoryData($category)
    {
        $query = $this->service->getQueryCategory($category);
        return $this->arrayData($query);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function arrayData($query)
    {
        foreach ($query as $item) {
            if(!$item->presetTranslate->count()){
                continue;
            }
            $this->data[$item->category_id]
            [$item->presetsCategoryTranslate[0]->title]
            [$item->presetTranslate[0]->title] = [
                'is_name' => $item->preset->is_name,
                'preset_id' => $item->presetTranslate[0]->preset_id,
                'src' => $this->gerSrc($item->presetTranslate[0]->src),
                'icon' => $item->preset->icon ? Storage::disk('preset')->url($item->preset->id . '/' . $item->preset->icon) : null
            ];
        }

        return $this->data;
    }

    public function gerSrc($src)
    {
        $explode = explode('/', $src);
        $catalog = '';
        foreach ($explode as $i => $item) {
            if($item === 'catalog'){
                $catalog = $explode[$i+1];
            }
        }

        if(count($explode) > 4) {
            $url = $explode[0] . '//' . $explode[2] . '/';
            $locale = app()->getLocale();
            $city = config()->get('app.city');
            $url .= ($locale === 'ru' ? '' : $locale. '/')  . ($city === 'kiev' ? '' : $city.'/') . 'catalog/' . $catalog . '/';

            for ($i = $locale === 'ru' ? 5 : 6; $i < count($explode); $i++){
                if($i == count($explode) - 1){
                    $url .= $explode[$i];
                }else{
                    $url .= $explode[$i] . '/';
                }
            }

            return $url;
        }

        return $src;
    }
}
