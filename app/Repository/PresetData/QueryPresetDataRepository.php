<?php

namespace App\Repository\PresetData;

use App\Models\PresetData;

class QueryPresetDataRepository
{
    /**
     * @var PresetData
     */
    private $query;

    /**
     * QueryPresetDataRepository constructor.
     */
    public function __construct()
    {
        $this->query = PresetData::with([
                'presetsCategoryTranslate' => function($q){
                    $q->where('lang', app()->getLocale());
                },
                'presetTranslate' => function($q){
                    $q->where('lang', app()->getLocale())->whereNotNull('has_products');
                },
                'preset'
            ]);
    }

    /**
     * @return mixed
     */
    public function getQueryMenu()
    {
        return $this->query
            ->menu()
            ->orderBy('preset_category_menu_order')
            ->orderBy('preset_menu_order')
            ->get();
    }

    /**
     * @return mixed
     */
    public function getQueryCategory($category)
    {
        return $this->query
            ->category()
            ->where('category_id',$category)
            ->orderBy('preset_category_category_order')
            ->orderBy('preset_category_order')
            ->get();
    }
}
