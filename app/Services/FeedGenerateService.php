<?php

namespace App\Services;

use App\Jobs\FbFeedJob;
use App\Jobs\GoogleFeedJob;
use App\Jobs\HotlineActionFeedJob;
use App\Jobs\HotlineFeedJob;
use App\Jobs\NpFeedJob;
use App\Jobs\PriceUaFeedJob;
use App\Jobs\RzFeedJob;

class FeedGenerateService
{
    public static function generate()
    {
        HotlineFeedJob::dispatch()->onQueue('long-running');
        HotlineActionFeedJob::dispatch()->onQueue('long-running');
        GoogleFeedJob::dispatch()->onQueue('long-running');
        FbFeedJob::dispatch()->onQueue('long-running');
        NpFeedJob::dispatch()->onQueue('long-running');
        PriceUaFeedJob::dispatch()->onQueue('long-running');
        RzFeedJob::dispatch()->onQueue('long-running');
    }
}
