<?php
namespace App\Services\OrderPayments;

use App\Models\Order;
use Illuminate\Support\Facades\Log;

class Payments
{
    private $config;
    private $login;
    private $password;
    private $eMailPartner;
    private $partnerID;
    private $url;

    /**
     * SoftlineSMS constructor.
     */
    public function __construct()
    {
        $this->config = config('alfabank');
        $this->url = $this->config['url'];
        $this->login = $this->config['login'];
        $this->password = $this->config['password'];
        $this->eMailPartner = $this->config['email'];
        $this->partnerID = $this->config['partner_id'];
    }

    public function createOrder(Order $order)
    {
        $data = [
            'mPhone' => $order->phone,//мобильный номер телефона клиента Банка в международном формате (пример, +380671111111)
            'panEnd' => $order->card_number,//4 последних цифры номера карты клиента, по которой будет оформлена рассрочка
            'orderId' => $order->id,//ИД заказа покупки в рассрочку, уникальный в пределах Партнера (может содержать цифры, буквы, символы)
            'orderSum' => (integer)($order->price_with_percent*100),//сумма покупки (в копейках)
            'orderTerm' => $order->period,//срок рассрочки (в месяцах)
            'callBackURL' => route('alfabank.callback'),
            'eMailPartner' => $this->eMailPartner,
        ];

        $result = $this->request('POST', 'createOrder/'.$this->partnerID, $data);

        return array_merge(
            $this->response($result),
            [
                'order_bank_id' => $result->messageId ?? $result['messageId'],
            ]
        );
    }

    public function getOrder(Order $order)
    {
        $result = $this->requestGetOrder('getOrder/'.$this->partnerID.'?messageId='.$order->order_bank_id);
        return $this->response($result);
    }

    public function cancelOrder(Order $order)
    {
        $data = [
            'messageId' => $order->order_bank_id,
            'cancelId' => $order->id,
        ];

        $result = $this->request('POST', 'cancelOrder/'.$this->partnerID, $data);
        return $this->response($result);
    }

    public function getGuarantee(Order $order)
    {
        $result = $this->requestGetOrder('getGuarantee/'.$this->partnerID.'?messageId='.$order->order_bank_id);
        return $this->response($result);
    }

    public function confirmOrder()
    {

    }

    private function response($result)
    {
        return [
            'status' => $this->validation($result['statusCode'] ?? 'Unknown status'),
            'statusCode' => $result['statusCode'] ?? 'Unknown status',
            'message' => $this->messages($result['statusCode'] ?? 'Unknown status')
        ];
    }

    private function validation($statusCode)
    {
        switch ($statusCode){
            case 'IN_PROCESSING':
            case 'PURCHASE_IS_OK':
            case 'INST_ALLOWED_OK':
            case 'CHECK_SMS_OK':
            case 'PRE_PURCHASE_IS_OK':
            case 'FINAL_ORDER_OK':
                $status = 200;;break;
            default:
                $status = $statusCode;break;
        }

        return $status;
    }

    private function messages($statusCode)
    {
        switch ($statusCode){
            case 'IN_PROCESSING':
                $message = __('alfabank_statuses.IN_PROCESSING');break;
            case 'NO_PARTNERID':
                $message = __('alfabank_statuses.NO_PARTNERID');break;
            case 'MATCH_ORDERID':
                $message = __('alfabank_statuses.MATCH_ORDERID');break;
            case 'INVALID_ORDERID':
                $message = __('alfabank_statuses.INVALID_ORDERID');break;
            case 'INVALID_MPHONE':
                $message = __('alfabank_statuses.INVALID_MPHONE');break;
            case 'INVALID_PANEND':
                $message = __('alfabank_statuses.INVALID_PANEND');break;
            case 'INVALID_ORDERSUM':
                $message = __('alfabank_statuses.INVALID_ORDERSUM');break;
            case 'INVALID_ORDERTERM':
                $message = __('alfabank_statuses.INVALID_ORDERTERM');break;
            case 'INVALID_CALLBACKURL':
                $message = __('alfabank_statuses.INVALID_CALLBACKURL');break;
            case 'ORDERSUM_EXCEEDS':
                $message = __('alfabank_statuses.ORDERSUM_EXCEEDS');break;
            case 'ORDERSUM_LOW':
                $message = __('alfabank_statuses.ORDERSUM_LOW');break;
            case 'PAC_NOT_SUCCESS':
                $message = __('alfabank_statuses.PAC_NOT_SUCCESS');break;
            case 'PAC_IS_REVERSED':
                $message = __('alfabank_statuses.PAC_IS_REVERSED');break;
            case 'PAC_DUP_COMPL':
                $message = __('alfabank_statuses.PAC_DUP_COMPL');break;
            case 'PAC_DAY_LMT_EXC':
                $message = __('alfabank_statuses.PAC_DAY_LMT_EXC');break;
            case 'PAC_AMOUNT_EXT':
                $message = __('alfabank_statuses.PAC_AMOUNT_EXT');break;
            case 'PAC_ERROR':
                $message = __('alfabank_statuses.PAC_ERROR');break;
            case 'PAC_NOT_FOUND':
                $message = __('alfabank_statuses.PAC_NOT_FOUND');break;
            case 'INVALID_EMAIL':
                $message = __('alfabank_statuses.INVALID_EMAIL');break;
            case 'NO_APP':
                $message = __('alfabank_statuses.NO_APP');break;
            case 'NO_CONTRAGENT_MPHONE':
                $message = __('alfabank_statuses.NO_CONTRAGENT_MPHONE');break;
            case 'NO_CARD_PANEND':
                $message = __('alfabank_statuses.NO_CARD_PANEND');break;
            case 'LOW_BALANCE':
                $message = __('alfabank_statuses.LOW_BALANCE');break;
            case 'INST_ALLOWED_FAIL':
                $message = __('alfabank_statuses.INST_ALLOWED_FAIL');break;
            case 'NO_PRODUCT':
                $message = __('alfabank_statuses.NO_PRODUCT');break;
            case 'CHECK_SMS_ERROR':
                $message = __('alfabank_statuses.CHECK_SMS_ERROR');break;
            case 'PURCHASE_IS_FAIL':
                $message = __('alfabank_statuses.PURCHASE_IS_FAIL');break;
            case 'PURCHASE_IS_FAIL_EX':
                $message = __('alfabank_statuses.PURCHASE_IS_FAIL_EX');break;
            case 'NO_IDS':
                $message = __('alfabank_statuses.NO_IDS');break;
            case 'CLIENT_NO_SEND_SMS':
                $message = __('alfabank_statuses.CLIENT_NO_SEND_SMS');break;
            case 'SYSTEM_ERROR':
                $message = __('alfabank_statuses.SYSTEM_ERROR');break;
            case 'FINAL_CANCEL_OK':
                $message = __('alfabank_statuses.FINAL_CANCEL_OK');break;
            case 'MORE_ORDERID':
                $message = __('alfabank_statuses.MORE_ORDERID');break;
            case 'REQUEST_NOT_MATCH':
                $message = __('alfabank_statuses.REQUEST_NOT_MATCH');break;
            case 'INST_ALLOWED_OK':
                $message = __('alfabank_statuses.INST_ALLOWED_OK');break;
            case 'CHECK_SMS_OK':
                $message = __('alfabank_statuses.CHECK_SMS_OK');break;
            case 'PURCHASE_IS_OK':
                $message = __('alfabank_statuses.PURCHASE_IS_OK');break;
            case 'PRE_PURCHASE_IS_OK':
                $message = __('alfabank_statuses.PRE_PURCHASE_IS_OK');break;
            case 'FINAL_ORDER_OK':
                $message = __('alfabank_statuses.FINAL_ORDER_OK');break;
            case 'PRE_PURCHASE_IS_FAIL':
                $message = __('alfabank_statuses.PRE_PURCHASE_IS_FAIL');break;
            case 'PRE_PURCHASE_IS_FAIL_EX':
                $message = __('alfabank_statuses.PRE_PURCHASE_IS_FAIL_EX');break;
            case 'GUARANTEE_IS_FAIL':
                $message = __('alfabank_statuses.GUARANTEE_IS_FAIL');break;
            case 'GUARANTEE_IS_OK':
                $message = __('alfabank_statuses.GUARANTEE_IS_OK');break;
            case 'GUARANTEE_MAIL_OK':
                $message = __('alfabank_statuses.GUARANTEE_MAIL_OK');break;
            case 'GUARANTEE_IS_EMPTY':
                $message = __('alfabank_statuses.GUARANTEE_IS_EMPTY');break;
            default:
                $message = __('alfabank_statuses.INVALID_STATUS');break;
        }

        return $message;
    }

    public function request(string $method, string $action, array $data = null)
    {
        try {
            $signature = $this->login.':'.$this->password;
            $url = $this->url.$action;

            if ($curl = curl_init()) {
                curl_setopt($curl,CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
                if($method == 'POST'){
                    curl_setopt($curl,CURLOPT_POST, 1);
                }
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_USERPWD, $signature);
                if($data){
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                }

                $out = curl_exec($curl);


                $result = json_decode(
                    $out,
                    true
                );

                curl_close($curl);
            }
            return $result;
        } catch (\Exception $e) {
            dd($e);
            Log::error('Расстрочка с Алфабанком не оформлена: ' . serialize($e));
            return [
                'statusCode' => 400,
                'messageId' => null,
            ];
        }
    }

    public function requestGetOrder(string $action)
    {
        try {
            $signature = $this->login.':'.$this->password;
            $url = $this->url.$action;

            if ($curl = curl_init()) {
                curl_setopt($curl,CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_USERPWD, $signature);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $out = curl_exec($curl);

                $result = json_decode(
                    $out,
                    true
                );

                curl_close($curl);
            }
            return $result;
        } catch (\Exception $e) {
            dd($e);
            Log::error('Расстрочка с Алфабанком не оформлена: ' . serialize($e));
            return [
                'statusCode' => 400,
                'messageId' => null,
            ];
        }
    }
}
