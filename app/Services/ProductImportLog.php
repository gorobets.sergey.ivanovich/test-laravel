<?php

namespace App\Services;

use App\Http\Resources\ProductImportLogListResource;
use App\Http\Resources\ProductImportLogResource;
use Illuminate\Http\Request;
use App\Models\ImportLog as ItemOrm;

class ProductImportLog extends ClientPagination
{
    const LIMIT_MAIN = 3;
    const LIMIT = 6;
    const ICON = 0;
    const CLOSE = 0;
    const PAGE = 1;
    const PER_PAGE = 15;
    const SORRT = 'asc';

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $items = [];
        $page = $request->page ?? self::PAGE;
        $limit = $request->perPage ?? self::PER_PAGE;
        $order = $request->sort ?? self::SORRT;

        $positions = ItemOrm::with('details', 'category')->orderBy('id',$order)->get();

        if (count($positions) > 0) {
            $items = $this->selfPagination($positions->all(), $page, 0, $limit, $limit);
        }

        return ProductImportLogListResource::collection($items)->additional([
            'page' => $page,
            'total' => count($positions),
            'perPage' => $limit,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $model = ItemOrm::with('details')->where('id',  $request->log)->first();

        return response()->json(new ProductImportLogResource($model), 200);
    }

    /**
     * @param Request $request
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function file(Request $request)
    {
        $item = ItemOrm::find($request->log);

        //TODO: do it!
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . 'import_'. $item->category->title . '.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        header('Access-Control-Allow-Origin: *'); // HTTP/1.0

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load(storage_path($item->file));

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
}
