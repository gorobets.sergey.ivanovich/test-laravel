<?php

namespace App\Services\XML;

class XmlManager
{
    /**
     * @var SimpleXMLElement
     */
    protected $xml;

    /**
     * Static method for init object
     *
     * @param array|null $array
     *
     * @return XmlManager
     */
    public static function make(?array $array = null, string $custom_key = 'e'): XmlManager
    {
        if ($array) {
            return (new self)->setData($array, $custom_key);
        }
        return new self;

    }

    /**
     * Construct of XmlManager
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * Create XML object https://www.php.net/manual/en/book.simplexml.php
     *
     * @return void
     */
    protected function init()
    {
        $this->xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><root></root>');
    }

    /**
     * Set array for convert to XML
     *
     * @param array $array
     * @param string $custom_key
     *
     * @return $this
     */
    public function setData(array $array = [], string $custom_key = 'e'): XmlManager
    {
        $xml = $this->xml;
        $this->arrayToXml($array, $xml, $custom_key);
        $this->xml = $xml;
        return $this;
    }

    /**
     * Show XML response in Browser
     *
     * @return string
     */
    public function output()
    {
        $this->setHeaders();
        return $this->asXML();
    }

    /**
     * Get XML string
     *
     * @return string
     */
    public function asXML()
    {
        return $this->xml->asXML();
    }

    /**
     * Set header for response
     *
     * @return void
     */
    public function setHeaders()
    {
        header('Content-Type: application/xml; charset=utf-8');
    }

    /**
     * Generate XML response
     *
     * @param array $array
     * @param $xml
     * @param string $custom_key
     *
     * @return void
     */
    protected function arrayToXml(array $array, &$xml,  $custom_key = 'e'): void
    {
        foreach ($array as $key => $value) {
            if(is_int($key)){
                $key = $custom_key;
            }
            if(is_array($value)){
                $label = $xml->addChild($key);
                $this->arrayToXml($value, $label, $key[0]);
            }
            else {
                $xml->addChild($key, htmlentities($value, ENT_COMPAT | ENT_XML1, 'UTF-8'));
            }
        }
    }
}
