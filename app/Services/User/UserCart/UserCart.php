<?php

namespace App\Services\User\UserCart;

use App\Models\UserCart as ItemOrm;
use App\Repository\Product\QueryProductRepository;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UserCart
{
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    private $client;

    private $userCart;
    private $userCartProducts;
    private $fillableArray;

    const PREFIX = 'USER_CART_LIST_';

    /**
     * @param string $guard
     */
    public function __construct(string $guard = 'client-api')
    {
        $this->client = auth($guard)->user();
        $this->userCart = $this->client ? $this->client->cart : null;

        if($this->userCart){
            $this->userCart->products->map(function ($item){
                $this->userCartProducts[$item->product_id] = [
                    'count' => (int)$item->count
                ];
            })->all();
        }

        $this->fillableArray = [
            'id', 'is_action', 'is_new', 'is_hit', 'url', 'show_price_sale', 'show_price_value', 'price', 'sale_price',
            'title', 'image', 'image-hover'
        ];
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json($this->getCartData(), 200);
    }

    /**
     * @return JsonResponse
     */
    public function link(): JsonResponse
    {
        $this->userCartProducts['expired_at'] = Carbon::createFromFormat('Y-m-d H:s:i', Carbon::now()->addDay())->toDateTimeString();

        return response()->json([
            'link' => route('cart',['hash' => base64_encode(json_encode($this->userCartProducts))])
        ], 200);
    }

    /**
     * @param array $cart
     * @return JsonResponse
     * @throws \Exception
     */
    public function add(array $cart): JsonResponse
    {
        if($this->client){
            if($this->userCart){
                $this->updateCart($cart);
            }else{
                $this->createCart($cart);
            }
        }

        return $this->response();
    }

    /**
     * @param array $cartt
     * @return void
     * @throws \Exception
     */
    private function createCart(array $cart):void
    {
        DB::beginTransaction();
        try {
            $this->setUserCartProducts($cart);
            $cart = ItemOrm::create([
                'user_id' => $this->client->id
            ]);
            $cart->productList()->sync($this->userCartProducts);
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
        }
    }

    /**
     * @param array $cart
     * @return void
     */
    private function updateCart(array $cart):void
    {
        $this->setUserCartProducts($cart);
        $this->userCart->productList()->sync($this->userCartProducts);
    }

    /**
     * @param int $item
     * @return JsonResponse
     */
    public function clear(): JsonResponse
    {
        if($this->client){
            $this->client->productCartList()->delete();
        }

        return $this->response();
    }

    /**
     * @return JsonResponse
     */
    public function response(): JsonResponse
    {
        $this->client = $this->client ? $this->client->fresh() : null;

        return $this->index();
    }

    /**
     * @param array $cart
     * @return JsonResponse
     * @throws \Exception
     */
    public function combine(array $cart): JsonResponse
    {
        if($this->userCartProducts){
            foreach ($cart as $items) {
                foreach ($items as $item) {
                    $existsId = 0;
                    foreach ($this->userCartProducts as $id => $count){
                        if($id == $item['id']){
                            $existsId = $item['id'];
                            break;
                        }
                    }
                    if($existsId > 0){
                        $this->userCartProducts[$item['id']]['count'] += $item['count'];
                    }else{
                        $this->userCartProducts[$item['id']]['count'] = $item['count'];
                    }
                }
            }
        }else{
            $this->createCart($cart);
        }

        return $this->response();
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getCartData(): array
    {
        $products = (new QueryProductRepository())->getProductWatched(array_keys($this->userCartProducts));

        $totalPrice = 0;
        foreach ($products as $i => $product) {
            $products[$i] = Arr::only($product, $this->fillableArray);
            $products[$i]['count'] = $this->userCartProducts[$product['id']]['count'];
            $totalPrice += ($product['sale_price']
                ? $product['sale_price']*$this->userCartProducts[$product['id']]['count']
                : $product['price']*$this->userCartProducts[$product['id']]['count']);
        }

        return [
            'products' => $products,
            'totalPrice' => $totalPrice
        ];
    }

    /**
     * @param array $cart
     * @return void
     */
    private function setUserCartProducts(array $cart): void
    {
        foreach ($cart as $items) {
            foreach ($items as $item) {
                $this->userCartProducts[$item['id']] = ['count' => $item['count']];
            }
        }
    }
}
