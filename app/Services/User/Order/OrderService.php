<?php

namespace App\Services\User\Order;

use App\Http\Resources\AuthClientOrderHistoryResource;
use App\Http\Resources\AuthClientOrderResource;
use App\Models\Entity\OrdersClientEntityInterface;
use App\Models\Entity\OrdersCustomerTypesEntity;
use App\Models\Entity\OrdersDeliveryTypesEntity;
use App\Models\Entity\OrdersPaymentTypesEntity;
use App\Models\OrdersClient;
use App\Models\OrdersEntityClientContact;
use App\Models\OrdersNewClientContact;
use App\Models\OrdersRegularClientContact;
use App\Repository\Product\ProductRepository;
use App\Services\User\UserCart\UserCart;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class OrderService
{
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    private $client;

    /**
     * @var OrdersClient
     */
    private $orderClient;

    private $customer;

    /**
     * @var array
     */
    private $formData;

    /**
     * @var array|null
     */
    private $cartProducts;

    /**
     * @var UserCart
     */
    private $userCartService;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct()
    {
        $this->client = auth('client-api')->user();
        $this->orderClient = new OrdersClient();
        $this->userCartService = new UserCart();
        $this->customer = null;
        $this->cartProducts = json_decode($this->userCartService->index()->getContent(), true);
        $this->productRepository = new ProductRepository();
    }

    /**
     * @return JsonResource
     */
    public function index(): JsonResource
    {
        $orders = $this->client->load([
            'orders', 'orders.regularCustomerContacts', 'orders.deliveryAddress',
            'orders.paymentDetails', 'orders.products'
        ]);

        return AuthClientOrderResource::collection($orders->orders);
    }

    /**
     * @param array $formData
     * @return JsonResponse
     * @throws \Exception
     */
    public function create(array $formData): JsonResponse
    {
        DB::beginTransaction();
        try {
            if (Arr::has($this->cartProducts,'products')){
                $this->formData = $formData;
                $this->saveOrder(Arr::only($this->formData,$this->orderClient->getFillable()));
                $this->saveCustomerData();
                $this->saveDeliveryType();
                $this->savePaymentDetails();
                $this->saveOrderDetails();
                $this->saveOrderAuthClient();
                $this->saveOrderHistory();

                DB::commit();
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 400);
        }

        return response()->json([new AuthClientOrderResource($this->orderClient)], 200);
    }

    /**
     * @param array $mainData
     * @return void
     */
    private function saveOrder(array $mainData): void
    {
        $mainData['status'] = OrdersClientEntityInterface::STATUS_NEW_ORDER;
        $this->orderClient = OrdersClient::create($mainData);
    }

    /**
     * @return void
     */
    private function saveCustomerData(): void
    {
        switch ($this->orderClient->client_type_id){
            case OrdersCustomerTypesEntity::NEW_CUSTOMER:
                $this->customer = new OrdersNewClientContact();
                $this->newCustomerOrder();
                break;
            case OrdersCustomerTypesEntity::REGULAR_CUSTOMER:
                $this->customer = new OrdersRegularClientContact();
                $this->regularCustomerOrder();
                break;
            case OrdersCustomerTypesEntity::ENTITY_CUSTOMER:
                $this->customer = new OrdersEntityClientContact();
                $this->entityCustomerOrder();
                break;
        }
    }

    /**
     * @return void
     */
    private function newCustomerOrder(): void
    {
        $this->orderClient->newCustomerContacts()->create(
            Arr::only($this->formData, $this->customer->getFillable())
        );
    }

    /**
     * @return void
     */
    private function regularCustomerOrder(): void
    {
        $addresses = $this->client->addresses->first();
        if ($addresses){
            $this->orderClient->regularCustomerContacts()->create(
                $addresses->toArray()
            );
        }else{
            $this->orderClient->regularCustomerContacts()->create([
                'user_id' => $this->client->id
            ]);
        }
    }

    /**
     * @return void
     */
    private function entityCustomerOrder(): void
    {
        $this->orderClient->entityCustomerContacts()
            ->create(Arr::only($this->formData, $this->customer->getFillable()));
    }

    /**
     * @return void
     */
    public function saveDeliveryType(): void
    {
        switch ($this->orderClient->delivery_type_id){
            case OrdersDeliveryTypesEntity::BY_COURIER:
            case OrdersDeliveryTypesEntity::MEEST:
                $this->orderClient->deliveryAddress()->create([
                    'delivery_type_id' => $this->orderClient->delivery_type_id,
                    'address' => $this->formData['delivery_address']
                ]);
                break;
        }
    }

    /**
     * @return void
     */
    public function savePaymentDetails(): void
    {
        if($this->orderClient->payment_type_id === OrdersPaymentTypesEntity::PAYMENT_IN_PARTS){
            $this->orderClient->paymentDetails()->create([
                'payment_type_id' => $this->orderClient->payment_type_id,
                'details' => $this->formData['payment_details']
            ]);
        }
    }

    /**
     * @return void
     */
    public function saveOrderDetails(): void
    {
        $products = $this->productRepository->prepareSeedForProductsIndex(array_column($this->cartProducts['products'],'id'));

        foreach ($this->cartProducts['products'] as $item) {
            $this->orderClient->products()->create([
                'product_id' => $item['id'],
                'article' => $products[$item['id']]['article'],
                'title' => $item['title'],
                'price' => $products[$item['id']]['price'],
                'price_sale' => $products[$item['id']]['sale_price'],
                'sale_with_price' => $products[$item['id']]['sale_with_price'],
                'count' => $item['count']
            ]);

            $this->orderClient->total_price += $products[$item['id']]['sale_with_price']
                ? $products[$item['id']]['sale_with_price'] * $item['count']
                : (
                    $products[$item['id']]['sale_price']
                    ? $products[$item['id']]['sale_price'] * $item['count']
                    : $products[$item['id']]['price'] * $item['count']
                );
        }

       $this->orderClient->update();
    }

    /**
     * @return void
     */
    public function saveOrderAuthClient(): void
    {
        $this->client->order()->create([
            'client_order_id' => $this->orderClient->id
        ]);
    }

    /**
     * @return void
     */
    public function saveOrderHistory(): void
    {
        $this->orderClient->history()->create([
            'status' => $this->orderClient->status
        ]);
    }
}

