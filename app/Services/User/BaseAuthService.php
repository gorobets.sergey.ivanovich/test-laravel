<?php

namespace App\Services\User;

class BaseAuthService
{
    protected $user;

    public function __construct()
    {
        $this->user = auth('client-api')->user();
    }
}
