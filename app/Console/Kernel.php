<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if(env('APP_ENV') == 'production'){
            $schedule->command('media:active:control')->everyMinute()->withoutOverlapping();
            $schedule->command('import:1c:price')->cron("3 9,13,17 * * *")->withoutOverlapping();
            $schedule->command('import:date:price')->everyMinute()->withoutOverlapping();
            $schedule->command('import:price')->everyTenMinutes()->withoutOverlapping();
            $schedule->command('action:control')->everyMinute()->withoutOverlapping();
            $schedule->command('export:product:deleted')->everyFiveMinutes()->withoutOverlapping();
            $schedule->command('google:counter')->daily()->withoutOverlapping();
            $schedule->command('insta:token')->daily()->withoutOverlapping();
            $schedule->command('insta:data')->dailyAt('15:00')->withoutOverlapping();
            $schedule->command('youtube:data')->dailyAt('15:00')->withoutOverlapping();
            $schedule->command('alfabank:order:control')->everyFiveMinutes()->withoutOverlapping();
            $schedule->command('product:archive:delete')->everyFiveMinutes()->withoutOverlapping();
            $schedule->command('import:products:process')->everyMinute()->withoutOverlapping();
            $schedule->command('products:tender')->everyMinute()->withoutOverlapping();
            $schedule->command('presset:control')->everyThirtyMinutes()->withoutOverlapping();
            $schedule->command('products:preorder')->everyMinute()->withoutOverlapping();
            $schedule->command('category:cache:filter')->everyFiveMinutes()->withoutOverlapping();
            $schedule->command('products:deadline')->everyMinute()->withoutOverlapping();
            $schedule->command('mongo:products:replacement')->weeklyOn(1, '5:00')->withoutOverlapping();
            $schedule->command('mysql:dump')->cron("0 11,15,19,23 * * *")->withoutOverlapping();
            $schedule->command('mysql:dump:supplier')->weeklyOn(1, '3:00')->withoutOverlapping();
            $schedule->command('feed:control:command')->hourly()->withoutOverlapping();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
