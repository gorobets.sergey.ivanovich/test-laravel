<?php

namespace App\Models;

use App\Models\Entity\UserProfileEntity;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $email
 * @property string|null $phone
 * @property string $password
 * @property string|null $name
 * @property string|null $surname
 * @property string|null $facebook_id
 * @property string|null $google_id
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFacebookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGoogleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'password', 'facebook_id', 'google_id', 'surname', 'phone_expired_at', 'reset_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'reset_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'phone_expired_at' => 'datetime',
    ];

    public function scopeFindFromEmail($q, $email)
    {
        return $q->where('email', $email);
    }

    public function scopeFindFromPhone($q, $phone)
    {
        return $q->where('phone', $phone);
    }

    public function scopeFindFromService($q, $service,$inputSocial)
    {
        return $q->where($service,$inputSocial);
    }

    public function scopeFindFromToken($q, $token)
    {
        return $q->where('reset_token',$token);
    }

    public function wishList(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\UserWishlist', 'user_wish_list', 'user_id', 'product_id')->withTimestamps();
    }

    public function productWishList(): HasMany
    {
        return $this->HasMany(UserWishlist::class);
    }

    public function watched(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\UserWatched', 'user_watched', 'user_id', 'product_id')->withTimestamps();
    }

    public function productWatched(): HasMany
    {
        return $this->HasMany(UserWatched::class);
    }

    public function cart(): HasOne
    {
        return $this->HasOne(UserCart::class);
    }

    public function productCartList()
    {
        return $this->cart->products();
    }

    public function addresses(): HasMany
    {
        return $this->HasMany(UserAddress::class);
    }

    public function profile(): HasOne
    {
        return $this->hasOne(UserProfileEntity::class);
    }

    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(OrdersClient::class, 'orders_auth_client', 'user_id', 'client_order_id');
    }

    public function order(): HasOne
    {
        return $this->hasOne(OrdersAuthClient::class);
    }

}
