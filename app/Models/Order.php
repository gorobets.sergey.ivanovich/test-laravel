<?php

namespace App\Models;

use App\Models\Entity\OrdersEntity;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property int $payment_type_id
 * @property int $product_id
 * @property string|null $title
 * @property string|null $email
 * @property string|null $comment
 * @property string|null $promo
 * @property string|null $article
 * @property string|null $name
 * @property string|null $phone
 * @property string|null $property
 * @property int $status
 * @property float|null $price
 * @property float|null $price_sale
 * @property float|null $sale_with_price
 * @property string|null $action
 * @property int $period
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $card_number
 * @property int $order_checked
 * @property string|null $get_order_info_at
 * @property string|null $get_order_info_status
 * @property string|null $order_cancel_status
 * @property string|null $order_guarantee_confirm_status
 * @property string|null $order_bank_id
 * @property float|null $price_with_percent
 * @property int|null $percent
 * @property int $pre_order
 * @property string|null $alfa_status_callback
 * @property int|null $product_case_id
 * @property int|null $is_custom
 * @property-read mixed $date
 * @property-read \App\Models\PaymentType|null $payment_type
 * @property-read \App\Models\Product|null $product
 * @property-read \App\Models\Product|null $productCase
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAlfaStatusCallback($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereArticle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCardNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereGetOrderInfoAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereGetOrderInfoStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereIsCustom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereOrderBankId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereOrderCancelStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereOrderChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereOrderGuaranteeConfirmStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePaymentTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePreOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePriceSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePriceWithPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereProductCaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePromo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereProperty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereSaleWithPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Order extends OrdersEntity
{

}
