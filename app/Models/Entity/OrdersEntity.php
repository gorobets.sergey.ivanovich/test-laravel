<?php

namespace App\Models\Entity;

/**
 * App\Models\Entity\OrdersEntity
 *
 * @property int $id
 * @property int $payment_type_id
 * @property int $product_id
 * @property string|null $title
 * @property string|null $email
 * @property string|null $comment
 * @property string|null $promo
 * @property string|null $article
 * @property string|null $name
 * @property string|null $phone
 * @property string|null $property
 * @property int $status
 * @property float|null $price
 * @property float|null $price_sale
 * @property float|null $sale_with_price
 * @property string|null $action
 * @property int $period
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $card_number
 * @property int $order_checked
 * @property string|null $get_order_info_at
 * @property string|null $get_order_info_status
 * @property string|null $order_cancel_status
 * @property string|null $order_guarantee_confirm_status
 * @property string|null $order_bank_id
 * @property float|null $price_with_percent
 * @property int|null $percent
 * @property int $pre_order
 * @property string|null $alfa_status_callback
 * @property int|null $product_case_id
 * @property int|null $is_custom
 * @property-read mixed $date
 * @property-read \App\Models\PaymentType|null $payment_type
 * @property-read \App\Models\Product|null $product
 * @property-read \App\Models\Product|null $productCase
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereAlfaStatusCallback($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereArticle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereCardNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereGetOrderInfoAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereGetOrderInfoStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereIsCustom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereOrderBankId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereOrderCancelStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereOrderChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereOrderGuaranteeConfirmStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity wherePaymentTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity wherePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity wherePeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity wherePreOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity wherePriceSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity wherePriceWithPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereProductCaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity wherePromo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereProperty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereSaleWithPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrdersEntity whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrdersEntity extends BaseEntityModel
{
    protected $table = 'orders';

    /**
     * PageEntity constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'article', 'name', 'phone', 'status', 'property', 'price', 'action', 'price_sale', 'email', 'promo',
        'comment', 'sale_with_price', 'product_id', 'payment_type_id', 'period', 'card_number', 'get_order_info_at',
        'order_checked', 'get_order_info_status', 'order_bank_id', 'order_cancel_status', 'order_guarantee_confirm_status',
        'price_with_percent', 'percent', 'pre_order', 'alfa_status_callback', 'is_custom', 'product_case_id', 'ipn',
        'passport', 'date_birthday', 'address'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function productCase()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_case_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function payment_type()
    {
        return $this->hasOne('App\Models\PaymentType', 'id', 'payment_type_id');
    }


}
