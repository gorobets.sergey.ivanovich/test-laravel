<?php

namespace App\Models\Entity;

class OrdersDeliveryTypesEntity extends BaseEntityModel
{
    protected $table = 'orders_delivery_types';

    const TYPES = [
        self::PICKUP,
        self::BY_COURIER,
        self::MEEST,
        self::UKRPOSHTA,
        self::JUSTIN,
        self::NP,
    ];

    const PICKUP = 1;
    const BY_COURIER = 2;
    const MEEST = 3;
    const UKRPOSHTA = 4;
    const JUSTIN = 5;
    const NP = 6;
}
