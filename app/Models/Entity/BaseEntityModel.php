<?php

namespace App\Models\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Str;

/**
 * App\Models\Entity\BaseEntityModel
 *
 * @property-read mixed $date
 * @method static \Illuminate\Database\Eloquent\Builder|BaseEntityModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseEntityModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseEntityModel query()
 * @mixin \Eloquent
 */
class BaseEntityModel extends Model
{
    const LENGTH = 50;

    /**
     * BaseEntityModel constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @param $query
     * @param array $ids
     * @return mixed
     */
    public function scopeIds($query, array $ids)
    {
        return $query->whereIn('id', $ids);
    }

    /**
     * @param array $items
     * @param int $perPage
     * @param null $page
     * @param array $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|LengthAwarePaginator
     */
    public function paginate($items, $perPage = 50, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    /**
     * @param $name
     * @param $extention
     * @param int $i
     * @return string
     * @throws \Exception
     */
    public function newName($name, $extention, $i = 0)
    {
        return $name.'_'.time().random_int(1,1000000).'_'.$i.'.'.$extention;
    }

    /**
     * @param $file
     * @param $lang
     * @param string $disk
     * @param bool $isProduct
     * @return mixed|string
     * @throws \Exception
     */
    public function uploadImage($file, $lang, string $disk = '', bool $isProduct = false)
    {
        $image = $this->image ?? null;

        if($file){
            $extension = $file->getClientOriginalExtension();
            $image = $this->newName($this->disk, $extension);

            $this->imageOrigin($file, $image, $lang, $disk, $isProduct);
        }

        return $image;
    }

    /**
     * @param $file
     * @param $image
     * @param $lang
     * @param $disk
     * @param bool $isProduct
     */
    protected function imageOrigin($file, $image, $lang, $disk, bool $isProduct = false)
    {
        $pattern = $disk ? '/'.$disk : $disk;

        if (!is_dir(storage_path('app/public/images/'.$this->disk.'/'.$this->id.'/'.$lang.$pattern))) {
            Storage::disk('public')->makeDirectory('images/'.$this->disk.'/'.$this->id.'/'.$lang.$pattern);
        }else{
            if(!$isProduct && $disk != "slider"){
                $this->deleteImage($lang, $pattern);
                Storage::disk('public')->makeDirectory('images/'.$this->disk.'/'.$this->id.'/'.$lang.$pattern);
            }
        }

        Storage::disk($this->disk)->put($this->id.'/'.$lang.$pattern.'/'.$image ,\File::get($file));
    }

    /**
     * @param string $title
     * @param int $i
     * @return string
     */
    public function slug($title = '', $i = 0)
    {
        $slug = $this->setSlug($title, $i);

        $check = self::where('slug', $slug)->where('id', '!=', $this->id)->exists();

        if($check){
            return $this->slug($slug, ++$i);
        }

        return $slug;

    }

    /**
     * @param string $title
     * @param int $i
     * @return string
     */
    public function newSlug($title = '', $i = 0)
    {
        $slug = $this->setSlug($title, $i);

        $check = self::where('slug', $slug)->where('id', '!=', $this->id)->exists();

        if($check){
            return $this->newSlug($slug, ++$i);
        }

        return $slug;

    }

    /**
     * @param $text
     * @param $i
     * @return string
     */
    public function setSlug($text, $i)
    {
        $suffix = $i > 0 ? '-'.$i : '';
        return Str::slug($text.$suffix, '-');
    }

    /**
     * @param string $lang
     * @param string $pattern
     */
    public function deleteImage(string $lang = '', string $pattern = '')
    {
        Storage::disk($this->disk)->deleteDirectory($this->id.'/'.$lang.$pattern);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getDateAttribute()
    {
        return Carbon::parse($this->created_at)->format('d-m-Y');
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fillableUpdated;
    }
}
