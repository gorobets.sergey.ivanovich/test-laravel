<?php

namespace App\Models\Entity;

class UserAddressEntity extends BaseEntityModel
{
    protected $table = 'user_addresses';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'city', 'street', 'apartments', 'full_name', 'phone', 'is_active'
    ];
}
