<?php

namespace App\Models\Entity;

use App\Models\OrdersClient;
use Illuminate\Database\Eloquent\Relations\HasOne;

class OrdersAuthClientEntity extends BaseEntityModel
{
    protected $table = 'orders_auth_client';

    /**
     * @var array
     */
    protected $fillable = [
        'client_order_id', 'user_id'
    ];

    /**
     * @return HasOne
     */
    public function order(): HasOne
    {
        return $this->hasOne(OrdersClient ::class, 'id', 'client_order_id');
    }

}
