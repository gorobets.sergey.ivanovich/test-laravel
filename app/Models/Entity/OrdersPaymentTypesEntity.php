<?php

namespace App\Models\Entity;

class OrdersPaymentTypesEntity extends BaseEntityModel
{
    protected $table = 'orders_payment_types';

    const TYPES = [
        self::UPON_RECEIPT,
        self::CASHLESS_PAYMENTS,
        self::BANK_CARD_ON_THE_SITE,
        self::PAYMENT_IN_PARTS
    ];

    const UPON_RECEIPT = 1;
    const CASHLESS_PAYMENTS = 2;
    const BANK_CARD_ON_THE_SITE = 3;
    const PAYMENT_IN_PARTS = 4;
}
