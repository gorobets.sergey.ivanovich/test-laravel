<?php

namespace App\Models\Entity;

class UserCartProductEntity extends BaseEntityModel
{
    protected $table = 'user_cart_product';

    /**
     * @var array
     */
    protected $fillable = [
        'cart_id', 'product_id', 'count'
    ];
}
