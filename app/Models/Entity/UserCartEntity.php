<?php

namespace App\Models\Entity;

class UserCartEntity extends BaseEntityModel
{
    protected $table = 'user_cart';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id'
    ];

    public function products()
    {
        return $this->HasMany('App\Models\UserCartProduct', 'cart_id','id');
    }

    public function productList()
    {
        return $this->belongsToMany('App\Models\UserCartProduct', 'user_cart_product', 'cart_id', 'product_id');
    }
}
