<?php

namespace App\Models\Entity;

class OrdersCustomerTypeTranslateEntity extends BaseEntityModel
{
    protected $table = 'orders_customer_type_translate';

    /**
     * @var array
     */
    protected $fillable = [
        'order_customer_type_id', 'lang', 'title'
    ];

}
