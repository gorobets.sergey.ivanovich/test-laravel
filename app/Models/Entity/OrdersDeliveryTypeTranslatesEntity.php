<?php

namespace App\Models\Entity;

class OrdersDeliveryTypeTranslatesEntity extends BaseEntityModel
{
    protected $table = 'orders_delivery_type_translates';

    /**
     * @var array
     */
    protected $fillable = [
        'delivery_type_id', 'lang', 'title', 'price'
    ];
}
