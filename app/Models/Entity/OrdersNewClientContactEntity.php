<?php

namespace App\Models\Entity;

use Illuminate\Support\Arr;

class OrdersNewClientContactEntity extends BaseEntityModel
{
    protected $table = 'orders_new_client_contacts';

    /**
     * @var array
     */
    protected $fillable = [
        'client_order_id', 'name', 'phone', 'email'
    ];

    /**
     * @return array
     */
    public function getFillable():array
    {
        return Arr::except($this->fillable, 'client_order_id');
    }
}
