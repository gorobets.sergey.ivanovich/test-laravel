<?php

namespace App\Models\Entity;

class OrdersHistoryStatusesEntity extends BaseEntityModel
{
    protected $table = 'orders_history_statuses';

    /**
     * @var array
     */
    protected $fillable = [
        'client_order_id', 'status'
    ];
}
