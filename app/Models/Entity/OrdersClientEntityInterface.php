<?php

namespace App\Models\Entity;

interface OrdersClientEntityInterface
{
    const STATUS_NEW_ORDER = 1;
    const STATUS_DELIVERY_IS_PLANNED = 2;
    const STATUS_IS_COMPLETED = 3;
    const STATUS_PROCESSED_AUTOMATICALLY = 4;
    const STATUS_DONE = 5;
}
