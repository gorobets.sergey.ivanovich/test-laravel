<?php

namespace App\Models\Entity;

class OrderPaymentDetailsEntity extends BaseEntityModel
{
    protected $table = 'orders_payment_details';

    protected $casts = [
        'details' => 'array'
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'client_order_id', 'payment_type_id', 'details'
    ];
}
