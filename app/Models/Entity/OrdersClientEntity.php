<?php

namespace App\Models\Entity;

use App\Models\OrderDetails;
use App\Models\OrderPaymentDetails;
use App\Models\OrdersDeliveryAddress;
use App\Models\OrdersEntityClientContact;
use App\Models\OrdersHistoryStatus;
use App\Models\OrdersNewClientContact;
use App\Models\OrdersRegularClientContact;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class OrdersClientEntity extends BaseEntityModel implements OrdersClientEntityInterface
{
    protected $table = 'orders_client';

    /**
     * @var array
     */
    protected $fillable = [
        'city_id', 'client_type_id', 'delivery_type_id', 'payment_type_id', 'status', 'comment', 'details', 'total_price'
    ];

    /**
     * @return HasOne
     */
    public function newCustomerContacts(): HasOne
    {
        return $this->hasOne(OrdersNewClientContact::class, 'client_order_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function entityCustomerContacts(): HasOne
    {
        return $this->hasOne(OrdersEntityClientContact::class, 'client_order_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function regularCustomerContacts(): HasOne
    {
        return $this->hasOne(OrdersRegularClientContact::class, 'client_order_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function deliveryAddress(): HasOne
    {
        return $this->hasOne(OrdersDeliveryAddress::class, 'client_order_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function paymentDetails(): HasOne
    {
        return $this->hasOne(OrderPaymentDetails::class, 'client_order_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function products(): HasMany
    {
        return $this->hasMany(OrderDetails::class, 'client_order_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function history(): HasMany
    {
        return $this->hasMany(OrdersHistoryStatus::class, 'client_order_id', 'id');
    }
}
