<?php

namespace App\Models\Entity;

use Illuminate\Support\Carbon;

class UserProfileEntity extends BaseEntityModel
{
    protected $table = 'user_profiles';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'surname', 'date_of_birth', 'female', 'phone', 'email', 'language'
    ];

    /**
     * @var array
     */
    protected $fillableUpdated = [
        'name', 'surname', 'date_of_birth', 'female', 'phone', 'email', 'language'
    ];

    public function getDateOfBirthAttribute( $value ): string
    {
        return Carbon::createFromTimeString($value)->format('Y-m-d');
    }
}
