<?php

namespace App\Models\Entity;

use App\Models\OrdersCustomerTypeTranslate;

class OrdersCustomerTypesEntity extends BaseEntityModel
{
    protected $table = 'orders_customer_types';

    const TYPES = [
        self::NEW_CUSTOMER,
        self::REGULAR_CUSTOMER,
        self::ENTITY_CUSTOMER,
    ];

    const NEW_CUSTOMER = 1;
    const REGULAR_CUSTOMER = 2;
    const ENTITY_CUSTOMER = 3;

    public function translate(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(OrdersCustomerTypeTranslate::class,'order_customer_type_id', 'id')
            ->where('lang', app()->getLocale());
    }
}
