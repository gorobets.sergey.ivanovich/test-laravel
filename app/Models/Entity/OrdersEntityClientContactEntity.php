<?php

namespace App\Models\Entity;

use Illuminate\Support\Arr;

class OrdersEntityClientContactEntity extends BaseEntityModel
{
    protected $table = 'orders_entity_client_contacts';

    /**
     * @var array
     */
    protected $fillable = [
        'client_order_id', 'name', 'phone', 'email', 'entity_title', 'entity_inn', 'entity_address'
    ];

    /**
     * @return array
     */
    public function getFillable():array
    {
        return Arr::except($this->fillable, 'client_order_id');
    }
}
