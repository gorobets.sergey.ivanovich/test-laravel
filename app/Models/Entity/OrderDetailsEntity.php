<?php

namespace App\Models\Entity;

class OrderDetailsEntity extends BaseEntityModel
{
    protected $table = 'order_details';

    /**
     * @var array
     */
    protected $fillable = [
        'client_order_id', 'product_id', 'article', 'title', 'price', 'price_sale', 'sale_with_price', 'count'
    ];
}
