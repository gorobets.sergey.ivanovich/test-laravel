<?php

namespace App\Models\Entity;

class OrdersPaymentTypeTranslatesEntity extends BaseEntityModel
{
    protected $table = 'orders_payment_type_translates';

    /**
     * @var array
     */
    protected $fillable = [
        'order_payment_type_id', 'lang', 'title'
    ];
}
