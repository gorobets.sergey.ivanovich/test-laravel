<?php

namespace App\Models\Entity;

class OrdersDeliveryAddressEntity extends BaseEntityModel
{
    protected $table = 'orders_delivery_address';

    protected $casts = [
        'address' => 'array'
    ];
    /**
     * @var array
     */
    protected $fillable = [
        'client_order_id', 'delivery_type_id', 'address'
    ];
}
