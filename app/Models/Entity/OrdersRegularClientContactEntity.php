<?php

namespace App\Models\Entity;

use Illuminate\Support\Arr;

class OrdersRegularClientContactEntity extends BaseEntityModel
{
    protected $table = 'orders_regular_client_contacts';

    /**
     * @var array
     */
    protected $fillable = [
        'client_order_id', 'user_id', 'city', 'street', 'apartments', 'full_name', 'phone', 'is_active'
    ];

    /**
     * @return array
     */
    public function getFillable():array
    {
        return Arr::except($this->fillable, 'client_order_id');
    }
}
