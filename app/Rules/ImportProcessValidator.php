<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class ImportProcessValidator implements Rule
{
    private $message;
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $processTemp = DB::table('import_product_job_temp')->count();
        $process = DB::table('import_product_job')->count();
        $data = DB::table('import_product_head')->count();

        if($processTemp || $process || $data){
            $this->message = 'Запущен процесс импорта. Подождите окончания.';
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
