<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ImportXmlValidator implements Rule
{
    private $message;
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(is_file($value)){
            $extention = $value->getClientOriginalExtension();
            $mimeType = $value->getMimeType();

            if(!in_array($extention,['xml']) || !in_array($mimeType, ['application/xml', 'text/xml'])) {
                $this->message = trans('validation.invalid.file.format');
                return false;
            }
        }elseif(is_string($value)) {
            $this->message = trans('validation.invalid.file.type');
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
