<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ImageValidator implements Rule
{
    private $message;
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(is_file($value)){
            $extention = $value->getClientOriginalExtension();
            $mimeType = $value->getMimeType();

            if(!in_array($extention,['png', 'gif', 'jpeg', 'jpg', 'PNG', 'GIF', 'JPEG', 'JPG', 'webp', 'WEBP', 'svg', 'SVG']) ||
                !in_array($mimeType, ['image/png', 'image/x-png', 'image/gif', 'image/jpeg', 'image/jpg', 'image/webp', 'image/svg+xml'])
            )
            {
                $this->message = trans('validation.invalid.file.format');
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
