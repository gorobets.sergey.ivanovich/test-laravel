<?php

namespace App\Rules;

use App\Models\Suppliers\Supplier;
use Illuminate\Contracts\Validation\Rule;

class ImportSupplierValidator implements Rule
{
    private $message;
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $exists = Supplier::find($value);
        if(!$exists){
            $this->message = 'Выбранное значение для id некорректно.';
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
