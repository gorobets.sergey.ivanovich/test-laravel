<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Intervention\Image\Facades\Image as ImageInt;

class ImageIconValidator implements Rule
{
    private $message;

    const WIDTH = 72;
    const HEIGHT = 72;
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(is_file($value)){
            $oldHeight = ImageInt::make($value)->height();
            $oldWidth = ImageInt::make($value)->width();

            if(abs($oldHeight - self::HEIGHT) > 5 || abs($oldWidth - self::WIDTH) > 5)
            {
                $this->message = 'Размеры иконки должны быть в пределах 72х72px (+/- 5px)';
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
