<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ImportValidatorImport implements Rule
{
    private $message;
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(is_file($value)){
            $extention = $value->getClientOriginalExtension();
            $mimeType = $value->getMimeType();

            if(!in_array($extention,['xls', 'xlsx']) || !in_array($mimeType, ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/zip'])) {
                $this->message = trans('validation.invalid.file.format');
                return false;
            }

            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($value);

            foreach (config('app.locales') as $lang) {
                $sheet = $spreadsheet->getSheetByName($lang);

                if (!$sheet) {
                    break;
                }

                $itemsSheet[$lang] = $sheet->toArray();
            }

            $productsTitle = [];
            $productsDescription = [];

            foreach (config('app.locales') as $locale) {
                $i = 0;
                foreach ($itemsSheet[$locale] as $items) {
                    if($i === 0){
                        $i++;continue;
                    }

                    $productsTitle[$items[0]][$locale] = $items[1];
                    $productsDescription[$items[0]][$locale] = strlen($items[2]) > 0 ? $items[2] : null;
                }
            }

            $chunk = config('import.chunk');
            $count = ceil(count($itemsSheet['ru']) / $chunk);
            $newItemsSheet = [];

            for ($i = 0; $i < $count; $i++) {
                $newItemsSheet[$i] = [
                    'ru' => array_slice($itemsSheet['ru'],$i*$chunk,$chunk+1),
                    'uk' => array_slice($itemsSheet['uk'],$i*$chunk,$chunk+1),
                ];
            }

            foreach ($newItemsSheet as $shits) {
                foreach ($shits as $locale => $items) {
                    foreach ($items as $i => $item) {
                        if(!trim($item[0]) || !trim($item[1])){
                            $this->message = 'Ошибка названия или артикула товара. ' . $locale .' - ' . $i . ' строка';
                            return false;
                        }
                    }
                }
            }
        }elseif (null === $value){
            return true;
        }elseif(is_string($value)) {
            $this->message = trans('validation.invalid.file.type');
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
