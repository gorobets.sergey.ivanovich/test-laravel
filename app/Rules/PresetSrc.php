<?php

namespace App\Rules;

use App\Models\Category;
use App\Models\Property;
use App\Models\PropertyValuesTranslate;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class PresetSrc implements Rule
{
    private $message;
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $url = explode('/', $value);
        $domain = explode('/', env('APP_URL'));

        if(count($url) < 6){
            $this->message = 'Введены ошибочные даненые';
            return false;
        }

        if(mb_strtolower($url[3]) == 'uk' || mb_strtolower($url[3]) == 'ru'){
            $url = array_merge(
                array_slice($url,0,3),
                array_slice($url,4)
            );
        }

        if($url[0] != $domain[0] || $url[1] != $domain[1] || $url[2] != $domain[2]){
            $this->message = 'Доменное имя указано неверно {'.env('APP_URL').'}';
            return false;
        }

        if($url[3] != 'catalog'){
            $this->message = 'Ошибка структуры данных';
            return false;
        }

        $category = Category::where('slug', $url[4])->first();
        if(!$category){
            $this->message = 'Ошибка выбора каталога';
            return false;
        }

        for($i = 5; $i < count($url); $i++){
            $data = explode('=', $url[$i]);
            if(count($data) < 2){
                $this->message = 'Ошибка фильтра';
                return false;
            }

            if($data[0] == 'sort' || $data[0] == 'price' || $data[0] == 'page'){
                continue;
            }

            $property = DB::table('category_property as cp')
                ->select('cp.property_id')
                ->join('properties as p',function ($join) use($data){
                    $join->on('p.id', '=', 'cp.property_id')
                        ->where('p.slug', $data[0]);
                })->where('cp.category_id', $category->id)->first();

            if(!$property){
                $this->message = 'Ошибка характеристики {'.$data[0].'}';
                return false;
            }

            foreach (explode('-or-', $data[1]) as $item) {
                $value = PropertyValuesTranslate::where(function ($q) use($property, $item){
                    $q->where('property_id', $property->property_id)
                        ->where('slug', $item);
                })->exists();

                if(!$value){
                    $this->message = 'Ошибка значения {'.$item.'}';
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
